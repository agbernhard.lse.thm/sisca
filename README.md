# SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)

Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)<br>
Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)

Author: Rudolf Huttary<br>
Contributors: Stefan Bernhard, Urs Hackstein, Alexander Mair, Matteo Sickenberger, Usman Shafiq<br>
Third party components: findjobj.m is programmed and copyright by Yair M. Altman, the Java Heap Cleaner is programmed and copyright by Davide Tabarelli and the mysql-connector is made by Oracle. For more information see our LICENSES folder in the Repository.

This program is free software: you can redistribute it and/or modify it under the terms 
of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. 
If not, see <http://www.gnu.org/licenses/>.

---

For documentations and instructions please visit the [WIKI](https://gitlab.com/agbernhard.lse.thm/sisca/-/wikis/home)

**If you use this code please cite:**  
*Huttary, Rudolf, et al. "Simulation, identification and statistical variation in cardiovascular analysis (SISCA)–A software framework for multi-compartment lumped modeling." Computers in Biology and Medicine 87 (2017): 104-123.*
