classdef DbObj < handle
    %% DbObj - super class for classes with db representation 
    %
    % DbObj properties:
    %  id
    %  name
    %  description
    %
    % instance methods:
    %   ToString            - string representation of object 
    %
    % class methods:
    %   ClassName           - retrieve classname
    %   List                - generates a list of known DbObj objects
    %   Open                - shows a dialog to read one or more database objects
    %   SelectDlg           - shows a modal dialog to select the 'id' of a db object 
    %

    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    
%% properties
    properties
        id = []; 
        name = ''
        description = ''
    end
    
%% instance methods
    methods
        function str = ToString(this, full)
        %% ToString = generic string representation of object  
        %   >> ms = MacSim(100); 
        %   >> obj.ToString;     outputs 'MacSim(100)'
        %   >> obj.ToString(1);  outputs 'MacSim(100)[name]'
        %   >> obj.ToString(2);  outputs 'MacSim(100)[name]{description}'
        %   >> ms = MacSim([100:103]); 
        %   >> obj.ToString;     outputs 'MacSim(100 101 102 103)'
        % Parameter: 
        %      'full' (optional, default = 2) 0: only id, 
        %                                     1: id+name, 
        %                                     2: id+name+description, 
        %                                     myString -> 'id+name+description - myString'
        
            if length(this) > 1 || isnumeric(this.id)
                nums = sprintf('%i ', [this.id]); 
            else
                nums = sprintf('%s ', this.id); 
            end
            nums = nums(1:end-1); 
            if nargin == 1
                full = 2; 
            end
            if nargin == 2 && ischar(full)
                strArg = full; 
                full = 3; 
            end
            if nargin == 2 && (~isnumeric(full) || isempty(intersect(full, [0 1 2 3])))  
                full = 0; 
            end
            switch(full)
                case 0
                    str = sprintf('%s(%s)', class(this), nums); 
                case 1
                    str= sprintf('%s(%s)[%s])', class(this), nums, sprintf('%s ', this.name)); 
                case 2
                    str = sprintf('%s(%s)[%s]{%s})', class(this), nums, sprintf('%s ', this.name), sprintf('%s ', this.description)); 
                case 3
                    str = sprintf('%s(%s)[%s]{%s}: %s', class(this), nums, sprintf('%s ', this.name), sprintf('%s ', this.description), strArg); 
            end
        end 
    end
    methods (Static, Access = private)
        function class = ClassName(method)
        %% ClassName - retrieve classname
        %              This unavoidable hack uses the commandline history
        %   >> class = DbObj.ClassName('Open'); 
        %   >> class = DbObj.ClassName('List'); 
        %   PARAMETER: 
        %    'method'  name of calling method; 
        
            javaHistory=com.mathworks.mlservices.MLCommandHistoryServices.getSessionHistory;
            lastCommand = javaHistory(end).toCharArray';
            [pos len] = regexpi(lastCommand, ['\w+.' method]); 
            if isempty(pos)
                error('DbObj.%s: this call works only from commandline', method); 
            end
            class = lastCommand(pos:len-length(method)-1); 
        end
    end
    methods (Static)
        function obj = Open(db)
        %% Open - shows dlg to read one or more objects from database
        %         !for commandline call only!
        %         uses the class name as table name
        % >> obj = MacSim.Open;                  % retrieve single MacSim object from db
        % >> obj = MacSim.Open(true);            % retrieve one or mores MacSim object(s) from db
        % >> obj = MacSim.Open(multiselect, db); % use given database connection 
        % PARAMETER:
        %   'multiselect' (optional, default: false) allow for vector objects
        %   'db'    (optional, default: AngioDB) use existing AngioDB object for database access
        
            if nargin < 1 
                db = AngioDB; 
            end
            class = DbObj.ClassName('Open'); 
            sc = [{class}; superclasses(class)]; 
            sc = sc(1:end-2); 
            for i=1:length(sc)
                res = sel(sc{i});
                if res > 0; 
                    for ii=length(res):-1:1
                        obj(ii) = eval(sprintf('%s([%s]);', class, sprintf('%i ', res(ii)))); 
                    end
                    return; 
                end
            end
            obj = []; 
            function res = sel(cl)
                res =  db.select(sprintf('show Tables where Tables_in_angiodb = "%s"', cl)); 
                if ~isempty(res)
                    res = DbObj.SelectDlg('Select Object', sprintf('Select * from %s', cl), [], db, [], [], true); 
                end
            end
        end
        function id = SelectDlg(title, select, columnwidth, db, btn_Text, btn_CB, multiselect)
        %% SelectDlg - shows modal Dialog for selection of a db object 
        %  >>  mid = DbObj.SelectDlg(title, select, [0.02 0.05 0.05 0.05 0.1 0.5, 0.3, 0.1], 'PreView', db); 
        % PARAMETER
        %   'db'          AngioDB object; 
        %   'title'       String shown as title of Dialog
        %   'select'      MySql SELECT expression to fill table
        %   'columnwidth' vector with relative 'width' values for table columns, [] allowed
        %   'btn_Text'    Caption of Button
        %   'btn_CB'      callback function for button (called with selected id)
        %   'multiselect' (optional, default = 0) 0 = Single select, 1 = multiple select
        
           if nargin < 7
               multiselect = 0; 
           end
           if multiselect
                multiselect = 2; 
           end
           if nargin < 4 || ~isa(db, 'AngioDB')
                db = AngioDB; 
           end
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions');
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1400)/2;
            else
                scrf = (scrsz(3)-1000)/2;
            end
            position = [scrf, (scrsz(4)-600)/2, 1200, 600];
            hDlg = figure('Name', ['DbObj.OpenDlg: ' title], 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off', 'WindowStyle', 'normal');
            
            %table
            [headers dbcontent] = db.select1(select, true);
            if size(dbcontent, 2) == 1 
                dbcontent = cell(size(headers)); 
            end

            table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
            table.setSelectionMode(multiselect);
            cm = table.getColumnModel;
            if nargin > 2 && ~isempty(columnwidth)
                count = min(cm.getColumnCount, length(columnwidth)); 
                w = cm.getTotalColumnWidth;
                for i = 1:count
                    cm.getColumn(i-1).setPreferredWidth(columnwidth(i)*w);
                end
            end
            ScrollPane = javax.swing.JScrollPane(table);
            set(hDlg, 'Userdata', table);
            btLinePropsPos = [[0.02 0.1] , 0.96,0.85];
            [~,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg);
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            warning('off', 'MATLAB:hg:JavaSetHGProperty');
            if ~strcmp(dbcontent{1}, '')
                set(table, 'MouseMovedCallback', @cbMouseMoved);  % for tooltip table 1
                set(table,'MousePressedCallback',@cbSelect);      % select on double click
            end
            table.setAutoCreateRowSorter(true);
            
            % buttons
            BtnWidth = 80; 
            uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                'Position', [position(3) - 1*BtnWidth 20 60 30], 'Callback', @cbCancel);
            bOk = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Open', ...
                'Position', [position(3) - 2*BtnWidth 20 60, 30], 'Callback', @cbOk, 'Enable', 'off');
            
            if nargin > 5 && ~isempty(btn_Text)
                bFcn = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', btn_Text, ...
                    'Position', [position(3) - 3*BtnWidth 20 60, 30], 'Callback', @cbBtn, 'Enable', 'off');
            else
                bFcn = bOk; 
            end
            
            id = 0; 
            uiwait(hDlg);
            
            
            function cbMouseMoved(varargin)
                %% cbMouseMoved - maintains tooltip
                event = varargin{2};
                row = table.rowAtPoint(event.getPoint);
                col = table.columnAtPoint(event.getPoint);
                value = table.getModel.getValueAt(row,col);
                if ~isempty(value)
                    if isjava(value)
                        value = value.toString;
                    elseif isnumeric(value)
                        value = num2str(value);
                    end
                else
                    value = '';
                end
                % Multiline tooltip
                j = 1;
                while j * 100 < size(value, 2)
                    value = sprintf('%s<br/>%s',value(1:j*100), value(j*100+1:size(value, 2)));
                    j = j+1;
                end
                table.setToolTipText(['<html> ' char(value)]);
            end
            function cbSelect(varargin)
                %% cbSelect - callback, copies params on double Click
                %
                set(bOk, 'Enable', 'on'); 
                set(bFcn, 'Enable', 'on'); 
                if size(varargin,2) == 2  % selection by double click
                    ev = varargin{1,2};
                    clicks = ev.getClickCount; 
                    if clicks > 1
                        cbOk(varargin); 
                    end
                end
            end
            function cbOk(varargin)
               row = table.getSelectedRows;
               for i = 1:length(row)
                    id(i) = table.getModel.getValueAt(row(i),0);
               end
               cbCancel(varargin); 
            end
            function cbBtn(varargin)
               row = table.getSelectedRows;
               for i = 1:length(row)
                    previd(i) = table.getModel.getValueAt(row(i),0);
               end
               btn_CB(previd); 
            end
            function cbCancel(varargin)
                delete(hDlg); 
            end
        end
        function res = List(varargin)
        %% List  - generates a list of known DbObj objects
        %          throws error if class name has no corresponding table in the database
        %          by default not more than 100 rows are listed
        %    >> DbObj.List(verbose, count, db); 
        %    >> all = MacSim.List;      % lists all in verbose mode and returns info in a struct array
        %    >> Measurement.List(true, 3);            % list the first 3 objects 
        %    >> Measurement.List(3);                  %   "
        %    >> all = Measurement.List(false);        % returns info in a struct array
        %       'verbose' (optional, 1st param, default: true) if true, output to command window
        %       'count'   (optional, 1st or 2nd param, default 100) restricts output to maximal 'count' objects; 
        %       'db'      (optional, last param: default = AngioDB) use the specified AngioDb object 
        
            class = DbObj.ClassName('List'); 
            % parameter
            verbose = true; 
            count = 100; 
            if nargin > 0 && isa(varargin{end}, 'AngioDB')
                db = varargin{1}; 
            else
                db = AngioDB; 
            end
            if nargin > 0 
                if islogical(varargin{1})
                    verbose = varargin{1}; 
                elseif isnumeric(varargin{1})
                    count = varargin{1};
                end
            end
            if nargin > 1 && isnumeric(varargin{2})
               count = varargin{2}; 
            end
            
            % read type info and rows
            Fields = db.select(['show fields from ' class]); 
            res = db.select(sprintf('select * from %s limit %i', class, count+1)); 
            if isempty(res)
                error('DbObj.List: table %s not found or empty', class); 
                return; 
            end
            len = length(res); 
            if nargin == 0
                verbose = true; 
            end
            len = min(count, len); 
            
            if verbose
                % table header line
                dec(length(Fields)) = 0;  % remember space per field
                for j=1:length(Fields)
                    t = Fields(j).Type; 
                    if strfind(t, 'blob')
                        continue; 
                    end
                    f = Fields(j).Field; 
                    pos = strfind(t, '('); 
                    if ~isempty(pos)
                        dec(j) = min(20, max(length(f), str2num(t(pos+1:end-1)))); 
                        fprintf(sprintf('%%%is | ', dec(j)), f(1 : min([end, dec(j), 30]))); 
                    else
                        dec(j) = 20; 
                        fprintf('%20s | ', f); 
                    end
                end
                lin = char(ones(1,(sum(dec)+length(dec)*3)-1)*'='); 
                fprintf('\n%s\n', lin); 
                
                %% value lines
                fnames = fields(res);
                for i=1: len
                    for j=1:length(Fields)
                        if strfind(Fields(j).Type, 'blob')
                            continue; 
                        end
                        if [strfind(Fields(j).Type, 'int') strfind(Fields(j).Type, 'double')]
                            val = sprintf(sprintf('%%%is', dec(j)), num2str(res(i).(fnames{j}))); 
                        elseif strfind(Fields(j).Type, 'datetime')
                            val = sprintf(sprintf('%%%is', dec(j)), char(res(i).(fnames{j}))); 
                        else
                            val = sprintf(sprintf('%%%is', dec(j)), char(res(i).(fnames{j}))); 
                        end
                        val(strfind(val, char(10))) = '*'; 
                        val(strfind(val, char(13))) = '*'; 
                        fprintf('%s | ', val(1:dec(j))); 
                    end
                    fprintf('\n'); 
                end
                if len < length(res)
                    fprintf('\n%s\nGeneric listing of the first %i elements of table ''%s'' in database ''%s''\n', lin, len, class, db.dbDb); 
                else
                    fprintf('\n%s\nGeneric listing of the Elements of table ''%s'' in database ''%s''\n', lin, class, db.dbDb); 
                end
                fprintf('\n'); 
            end
        end
    end
    
end

