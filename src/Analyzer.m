classdef Analyzer < handle
    %% Analyzer class implements a GUI based tool for the interactive analysis of 
    %     StatisticalSets or arbitrary ParamSet collections over an ArteryNet object
    %  Author: Rudolf Huttary, 10.2011
    %   features: 
    %      one/two node(s) selectable
    %      each axes pair is scalable by mousewheel after selection on mouseclick
    %
    % Analyzer Properties:
    %
    % class methods:
    %   
    % instance methods:
    %       Analyzer    - constructor with overloads for statistical set and parameter lists
    %       Show        - openes Analyser window and initializes GUI functionality
    
    % SISCA (Simulation, Identification and Statistical variation in Cardiovascular Analysis)
    %
    % Copyright (C) 2010 - 2018 FU Berlin (Mathematics, workgroup Prof. Christof Schütte)
    % Copyright (C) 2019 - 2020 THM Giessen (LSE, workgroup Prof. Stefan Bernhard)
    %
    % Author: Rudolf Huttary
    %
    % This program is free software: you can redistribute it and/or modify it under the terms 
    % of the GNU Affero General Public License Version 3 as published by the Free Software Foundation.
    %
    % This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    % without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    % See the GNU Affero General Public License for more details.
    %
    % You should have received a copy of the GNU Affero General Public License along with this program. 
    % If not, see <http://www.gnu.org/licenses/>.
    properties 
        arteryNet; 
        currentStatisticalSet;
        paramSetList; 
        psRef;
        psSet;
        db; 
        colsPerRow = 30; 
        selection    % current selection of ParamSets (index list relative to this.psSet)
        fatness = 1; 
        hClassifierObj;
        
        refStep = 10; 
    end
    properties (Access = private)
        %% Analyzer variables
        % misc Analyzer Figure
        selection1  % backup copy used by HistoPlot (value plot)
        timerCellhDlg;
        timerCellhCompare;
        timerCellColor;
        statisticalSets;
        selectionIndices;
        selectedStatisticalSets;
        loadedSolutions;
        loading = true;
        
        % UI elements Analyzer Figure
        hDlg 
        hAxes
        hHistoPanel 
        hPlotPanelF 
        hPlotPanelP 
        hNetPanel
        hPropVal
        hPropText
        hSelectedBins 
        hTable 
        hHistoButton
        
        
        % menu stuff Analyzer Figure
        hFileMenu
        hViewMenu
        hWindowsMenu
        hcmValuePlot
        hcmHistogram
        hcmNormalize
        hcmFFT 
        hcmFFTi 
        hcmPowerSpec
        hcmStdDev 
        
        % views
        vNormalize = false; 
        vValueplot = false; 
        vVisualize = false; 
        vGradient  = false; 
        vPowerSpec = false; 
        vFFT       = false; 
        vFFTi      = false; 
        vPeaks     = false;
        vLatency   = false; 
        vStdDev    = false;
        vGrid      = false;
        vLoad      = true;
        vHistomode = 0;
        vDistinguishBetween = 0;
        
        % state
        data; 
        ids; 
        currentNode 
        currentNodeSecondary
        
        %% Compare variables
        % misc Compare Figure
        CovTableData;
        brushSelection;
        plotSelection;
        lastClickedPlot;
        dimensionList;
        diagnoses;
        refLen;
        refT;
        refFreq;
        delta;
        div = 5;
        timeFreq;
        
        %UI eLements Compare Window Figure
        hCompare
        hCovMatPanel
        hCovPanelF
        hCovPanelP
        hStdDevPanelF
        hStdDevPanelP
        hCovTable
        hPlotF
        hPlotP
        hFeaturePanel
        hControlPanel
        hFeatureAddButton
        hScaleButton
        hFeatureAxes
        hScatter
        hXAxisDropDown
        hYAxisDropDown
        
        %menu stuff compare figure
        hClassifierMenu
        hFileMenuCompare
        hFeatureViewMenu
        hCompareViewMenu
        hcmFFTCompare 
        hcmFFTiCompare 
        hcmPowerSpecCompare
        hcmDistinguishSets
        hcmDistinguishDiagnoses
        hcmActualDistribution
        hcmGaussianDistribution
        hcmGrid
      
        %views Compare Figure
        vActualDistribution   = false;
        vGaussianDistribution = false;
        vMean                 = false;
        vDeviation            = false;
        vLinkBrushUD          = false;
        vLinkBrushLR          = false;
        vGradientCompare      = false;
        vPowerSpecCompare     = false; 
        vAmplitudeSpec        = false; 
        vPhaseSpec            = false; 
        vXAxis                = 1;
        vYAxis                = 1;
        
        %state variables compare figure
        firstSet
        secondSet
        firstDiagnose
        secondDiagnose
        setIndices
        diagnoseIndices
    end
    
    methods 
        function this = Analyzer(varargin)
        %% Analyzer - constructor with overloads for statistical set and parameter lists
        %             providing list with more the one element parameter uses
        %   >> this = Analyzer(ssId);                   %  instantiate to analyze StatisticalSet 
        %   >> this = Analyzer(ssId, db);               %    ... , using db
        %   >> this = Analyzer([ps1 plist]);        %  instantiate to analyze a set of ParamSets
        %   >> this = Analyzer([ps1 plist], db);    %  instantiate to analyze a set of ParamSets
        %     PARAMS:
        %          - 'db': optional last parameter, denotes an AngioDB object
        %          - 'ssId': DB-Identifier of an existing StatisticalSet object
        %          - 'ps1': DB-Identifier of ParamSet object to be used as reference element
        %          - 'plist': non empty list of DB-Identifiers of ParamSet objects to be analyzed 
        
            nar = nargin;
            if nar > 0 
                if isa(varargin{nargin}, 'AngioDB')
                    nar = nar - 1; 
                    if nar == 0
                        err; 
                    end
                    this.db = varargin{nargin}; 
                else
                    this.db = AngioDB; 
                end
            else
                error(sprintf('Analyzer: Parameter error:\n %s', help('Analyzer.Analyzer')));
            end
%             if nar > 1 || ~isnumeric(varargin{1})
%                 %HIER GEHTS LOS
%                 %error('Analyzer.Analyzer: to many arguments!\nUSAGE:\n%s', help('Analyzer.Analyzer'));
%                 this.currentStatisticalSet = StatisticalSet('Id', varargin{1}, this.db);   
%                 this.arteryNet = this.currentStatisticalSet.arteryNet; 
%                 this.psSet = this.currentStatisticalSet.Elements; 
%                 this.psRef = this.currentStatisticalSet.getSeed; 
%             end
            if length(varargin{1}) > 1  % we analyze a set of ParamSet objects
                % test, if all PS exist and refer to the same ArteryNet
               this.paramSetList = varargin{1}; 
               res = this.db.select(sprintf('select arteryNetId as aid from ParamSet where id = %i', this.paramSetList(1))); 
               if isempty(res) 
                error('Analyzer.Analyzer: first ParamSet object not found in DB'); 
               end
               aid = res.aid; 
               select = sprintf('select count(id) as len from ParamSet where arteryNetId = %i and id=%i %s group by arteryNetId', aid, this.paramSetList(1), sprintf('or id = %i ', this.paramSetList(2:end)));
               res = this.db.select(select); 
               if isempty(res) || length(res) > 1 || res.len ~= length(this.paramSetList)
                    error('Analyzer.Analyzer: not all ParamSet objects exist or refer to the same ArteryNet objects'); 
               end
               this.arteryNet = ArteryNet(aid, this.paramSetList(1)); 
               this.psRef = this.arteryNet.getParamSet; 
               sel = sprintf('select id, currentSolverRun, name, description from ParamSet where id = %i', this.paramSetList(2));
               if length(this.paramSetList) > 2
                   sel = [sel sprintf(' or id=%i', this.paramSetList(3:end))] 
               end
               this.psSet = this.db.select(sel);        
            else       
                this.currentStatisticalSet = StatisticalSet('Id', varargin{1}, this.db);   
                this.arteryNet = this.currentStatisticalSet.arteryNet; 
                this.psSet = this.currentStatisticalSet.Elements; 
                this.psRef = this.currentStatisticalSet.getSeed; 
                this.statisticalSets = cell2mat(varargin);
                order = [];
                counter = 1;
                sorted = sort(cell2mat(varargin));
                for i = cell2mat(varargin)
                    order(counter) = find(sorted == i);
                    counter = counter + 1; 
                end
                sel = sprintf(',%i', cell2mat(varargin)); 
                sel = sel(2:end);
                res = this.db.select(['select diagnoseId as id, Diagnose.name from StatisticalSet join Diagnose on StatisticalSet.diagnoseId = Diagnose.id where StatisticalSet.id in(',sel,')']);
                if ~isempty(res)
                    s.id = [res.id];
                    s.name = {res.name};
                    s.id = s.id(order);
                    s.name = s.name(order);
                    this.diagnoses = s;
                end
            end
            this.psRefInit; 
            this.Show; 
        end
        
        function psRefInit(this)
            this.psRef.solution = this.arteryNet.CurrentSolution; 
            % read CTS for psRef
            sel = sprintf('SELECT paramSetId, heartRate, unit, rate, data FROM NetParam join CTS on inboundCtsId = CTS.id WHERE paramSetId = %i', this.psRef.id); 
            sol0 = this.db.select(sel); 

            for i=1:length(sol0)
                data2 = typecast(sol0(i).data', 'double');
                data1 = zeros(1,length(data2));  % flow
                this.psRef.CTS.data = [data1; data2]; 
                this.psRef.CTS.duration = 60./ double(sol0(i).heartRate);
                delta = this.psRef.CTS.duration/length(data2); 
                this.psRef.CTS.time = 0:delta:this.psRef.CTS.duration-delta; 
            end
            RCL = this.arteryNet.RCL; 
            % calc R, C, L, Ao, Co
            for i=length(RCL.R):-1:1
                this.psRef.nodeParam(i).R = RCL.R(i); 
                this.psRef.nodeParam(i).C = RCL.C(i); 
                this.psRef.nodeParam(i).L = RCL.L(i); 
                this.psRef.nodeParam(i).Ao = RCL.Ao(i); 
                this.psRef.nodeParam(i).co = RCL.co(i); 
            end
            % transfer boundary values R, C, L, p, q (used in Histoplot)
            for i=length(this.psRef.boundaryParam):-1:1
                j = this.psRef.boundaryParam(i).nodeId; 
                this.psRef.nodeParam(j).R_b = this.psRef.boundaryParam(i).R; 
                this.psRef.nodeParam(j).C_b = this.psRef.boundaryParam(i).C; 
                this.psRef.nodeParam(j).L_b = this.psRef.boundaryParam(i).L; 
                this.psRef.nodeParam(j).p_b = this.psRef.boundaryParam(i).p; 
                this.psRef.nodeParam(j).q_b = this.psRef.boundaryParam(i).q; 
            end
        end % ctor
         
        function Show(this)
        %% Show - openes Analyser window and initializes GUI functionality
        %   implicitly called by the constructor        
            obj = this.arteryNet; 
            this.timerCellhDlg = timer;  %#ok<*CPROP,PROP>
            this.timerCellColor = timer;
            
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions'); 
            if size(mp,1) > 1
                scrf = 1.017 * scrsz(3); 
            else
                scrf = 0.017 * scrsz(3); 
            end
            pos = [scrf scrsz(4)/44  16*scrsz(4)/11   10*scrsz(4)/11];
            titl = sprintf('ArteryNet(%i, %i, %i)',obj.id, obj.p_id, obj.p_solverRun.id);  
            if ~isempty(this.currentStatisticalSet)
               strSets = sprintf('%i,',this.statisticalSets);   
               titl = sprintf('%s over StatisticalSet(%s)', titl,  strSets(1:end-1));  
            else
               titl = sprintf('%s over ParamSets [%s]', titl, sprintf('%i ', this.paramSetList));  
            end
            if isempty (this.hDlg) || ~ishandle(this.hDlg)
                this.hDlg = figure('Name', ['Analyzer - ' titl], 'NumberTitle', 'off', 'WindowStyle', 'normal', 'MenuBar','none', 'ToolBar','none', ...
                     'Position',pos, 'WindowScrollWheelFcn', @scale, 'ToolBar','none', 'DeleteFcn', @cbDel);
            else
                guiobj = findobj(this.hDlg); 
                delete(guiobj(2:end)); 
            end
            ratio = 0.45; 
            this.hNetPanel = uipanel('Parent', this.hDlg, 'Title', '',  'Units', 'normalized', 'Position', [0 0.15 0.4 0.85]);
            this.hAxes = axes('Parent', this.hNetPanel, 'Xlim', obj.axesRect(1:2)*0.9, 'Ylim', obj.axesRect(3:4)*0.9, 'XTick', [], 'YTick', [], ...
                'Position', [0 0 1 1]);
            obj.root.add2Dlg(this.hNetPanel, @this.nodeclick);   % populate with nodes & register callback for mouseclick
            
            % register cb also for root
            set(obj.root.hSelectText, 'ButtonDownFcn', @this.nodeclick); 
            set(obj.root.hSelect, 'ButtonDownFcn', @this.nodeclick); 
            
            this.hHistoPanel = uipanel('Parent', this.hDlg, 'Title', '',  'Units', 'normalized', 'Position', [0.4 0.75 0.6 0.2]);
            this.hPlotPanelF = uipanel('Parent', this.hDlg, 'Title', '',  'Units', 'normalized', 'Position', [0.4 0.45 0.6 0.3]);
            this.hPlotPanelP = uipanel('Parent', this.hDlg, 'Title', '',  'Units', 'normalized', 'Position', [0.4 0.15 0.6 0.3]);
                
            m = 1;
            for n = 0:0.1:(length(this.statisticalSets)/10)-0.1
                uicontrol(this.hDlg, 'Style', 'Pushbutton', 'String', this.statisticalSets(m), 'Units', 'normalized', 'Position', [0.4+n 0.95 0.1 0.05], 'Callback', @cbChangeStatisticalSet);
                m = m + 1;
            end
            hCurrentStatisticalSetButton = findall(gcf, 'Type', 'uicontrol', 'String', string(this.currentStatisticalSet.getId));
            set(hCurrentStatisticalSetButton, 'FontWeight', 'Bold','BackgroundColor', [0.65 0.65 0.65])
            this.hHistoButton = uicontrol(this.hHistoPanel, 'Style', 'Pushbutton', 'String', '>', 'Units', 'normalized', 'Position', [0 0 .03 .1], 'Callback', @this.circleHistogram ); 
         
            % add context menu
            this.hFileMenu = uimenu(this.hDlg, 'Label', 'File');
            this.hViewMenu = uimenu(this.hDlg, 'Label', 'View');           
            this.hWindowsMenu = uimenu(this.hDlg, 'Label', 'Windows');
            uimenu(this.hFileMenu, 'Label', 'Save selected solutions as...', 'Callback', @this.cbSaveData);
            uimenu(this.hFileMenu, 'Label', 'Load current node solution into local workspace','Separator', 'on', 'Checked', 'on', 'Callback', @cbLoadSolution2Workspace);
            uimenu(this.hViewMenu, 'Label', 'Visualize', 'Callback', @this.cmVisualize);
            uimenu(this.hViewMenu, 'Label', 'Bold', 'Callback', @this.cmFatness);
            this.hcmValuePlot = uimenu(this.hViewMenu, 'Separator', 'on', 'Label', 'Value Plot of selected', 'Callback', @this.cmValuePlot);
            this.hcmHistogram = uimenu(this.hViewMenu, 'Label', 'Histogram', 'Checked', 'on', 'Callback', @this.cmHistogram);
            this.hcmNormalize = uimenu(this.hViewMenu, 'Separator', 'on', 'Label', 'Normalize solution', 'Callback', @this.cmNormalize);
            uimenu(this.hViewMenu, 'Label', 'Gradient', 'Checked', 'off', 'Callback', @this.cmGradient);
            this.hcmPowerSpec = uimenu(this.hViewMenu, 'Label', 'Power spectrum', 'Checked', 'off', 'Callback', @this.cmPowerSpec); 
            this.hcmFFT = uimenu(this.hViewMenu, 'Label', 'FFT real coefficents', 'Checked', 'off', 'Callback', @this.cmFFT);
            this.hcmFFTi = uimenu(this.hViewMenu, 'Label', 'FFT imaginary coefficents', 'Checked', 'off', 'Callback', @this.cmFFTi);
            this.hcmStdDev = uimenu(this.hViewMenu, 'Label', 'Standard deviation', 'Checked', 'off', 'Callback', @this.cmStdDev);
            uimenu(this.hViewMenu, 'Label', 'Peaks & valleys', 'Checked', 'off', 'Enable', 'off', 'Callback', @this.cmPeaks);
            uimenu(this.hViewMenu, 'Label', 'Latency', 'Checked', 'off', 'Enable', 'off', 'Callback', @this.cmLatency);
            uimenu(this.hViewMenu, 'Separator', 'on', 'Label', 'Select All', 'Callback', @this.cmSelectAllTableFilter);
            uimenu(this.hViewMenu, 'Label', 'Remove Table Filter', 'Callback', @this.cmRemoveTableFilter);
            this.hcmGrid = uimenu(this.hViewMenu, 'Separator', 'on', 'Label', 'Take only every 10th value', 'Callback', @this.cbSetRefLen);
           
            
            uimenu(this.hWindowsMenu, 'Label', 'Compare', 'Callback', @this.ShowCompare);

           
            % prepare Paramset selector table
            this.initdata; 
            this.ids = [this.psSet.id]; 
            this.hTable = uitable(this.hDlg, 'Units', 'normalized', 'Position', [0 0 1 0.15], 'CellSelectionCallback', @cbSelection, ...
                'ColumnWidth', {pos(3)/this.colsPerRow}, 'ColumnName', '', 'RowName', '', ...
                'Data', this.data); 
            
            minima = zeros(1,length(this.statisticalSets));
            for setCounter = 1:length(this.statisticalSets)
                tmpStatisticalSet = StatisticalSet('Id', this.statisticalSets(setCounter), this.db);
                this.psSet = tmpStatisticalSet.Elements; 
                this.psRef = tmpStatisticalSet.getSeed;
                this.psRefInit;
                sol = this.readNodeSolutions(1);
                minimum = size(sol(1).data,2);       
                for solCounter = 2:length(sol)
                    val = size(sol(solCounter).data,2);
                    if val < minimum
                        minimum = val;
                    end
                end
                minima(setCounter) = minimum;
            end
            refLen = min(minima);
            this.refLen = refLen;
            this.refT = linspace(0, 1, this.refLen);
            this.delta = 1.0/(this.refLen-1);
            
            % select first node
            this.currentNode = this.arteryNet.nodeArr(1); 
            this.nodeclick; 
            
            
            set(this.hDlg, 'ResizeFcn', @resize); 
            % ----------------------------------------------------
            function cbDel(varargin)
                delete (this.timerCellhDlg);
                delete (this.timerCellColor);
                this.cbDelCompare;
            end
            
            function scale(h, evnt)
            %% scale - callback for mousewheel scaling
            % transforms axes relativly to mouse position
                fac = 1 + 0.03*evnt.VerticalScrollAmount; 
                if (evnt.VerticalScrollCount<0)  
                    fac = 1/fac; 
                end
                fac = fac - 1; 
                pos = get(gca, 'CurrentPoint');
                x = xlim; 
                y = ylim; 
                if pos(1) < x(1) || pos(1) > x(2) || pos(3) < y(1) || pos(3) > y(2) 
                    return;
                end
                x = x + (x-pos(1)) * fac; 
                y = y + (y-pos(3)) * fac; 
                xlim(x); 
                parent = get(gca, 'Parent'); 
                if parent == this.hNetPanel
                    ylim(y); 
                end
            end

            function cbSelection(varargin)
            % cbSelection - callback registered to keep track of the cell selection state of this.hTable
                ind = varargin{2}.Indices;
                if isempty(ind)
                    return;
                else
                    this.selectionIndices = ind;
                end
                sel = []; 
                for j=size(ind, 1):-1:1
                    val = this.data{ind(j,1), ind(j,2)}; 
                    if ~isempty(val) 
                        if isnumeric(val)
                            sel(j) = find(this.ids == val); 
                        else
                            sel(j) = find(this.ids == str2double(val(57:60)));
                        end
                    end
                end
                if ~isempty(sel)
                    this.selection = sel; 
                    this.selection1 = sel;  
                end
                stop(this.timerCellhDlg); 
                set(this.timerCellhDlg,'TimerFcn',@(x,y)this.nodeclick,'StartDelay',1)
                start(this.timerCellhDlg); 
                
                data = this.data;
                colergen = @(color,text) ['<html><table border=0 width=400 bgcolor=',color,'><TR><TD>',text,'</TD></TR> </table></html>'];
                if ~isempty(this.selection) && ~isempty(ind)
                    for i = 1:length(this.selection)
                        index = ind(i,:);
                        if ~isempty(data{index(1),index(2)})
                            data{index(1),index(2)} = colergen('#75C0F9',mat2str(data{index(1),index(2)}));
                        end
                    end
                end
                stop(this.timerCellColor); 
                set(this.timerCellColor,'TimerFcn',@(x,y)set(this.hTable,'Data', data),'StartDelay',1)
                start(this.timerCellColor);
                
                if ~isempty(this.hCompare)
                    stop(this.timerCellhCompare); 
                    set(this.timerCellhCompare,'TimerFcn',@(x,y)this.ComparePlot,'StartDelay',1)
                    if ~isempty(this.selectedStatisticalSets)
                        start(this.timerCellhCompare);
                    end
                end
            end   
            
            function cbChangeStatisticalSet(varargin)
                this.firstSet = str2double(varargin{1}.String);
                this.secondSet = this.currentStatisticalSet.getId;
                this.cmChangeStatisticalSet;
            end
            
            function resize(h, varargin)
            %% resize - callback treats resizing of figure while trying to keep aspect
                p = get(h, 'Position'); 
                colw = max(p(3)/this.colsPerRow-1, 35); 
                set(this.hTable, 'ColumnWidth', {colw}); 
                axes(this.hAxes); 
                ratio1 = p(3)/ p(4) * ratio; 
                x = xlim; 
                y = ylim;
                ratio2 = (x(2) - x(1))/(y(2)-y(1));
                if x>y
                  xlim(x * ratio1 * ratio2);
                else
                  ylim(y / ratio1 * ratio2);
                end
                this.placeProps; 
            end
            
            function cbLoadSolution2Workspace(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'off')
                    set(m, 'Checked', 'on'); 
                    this.vLoad = true;
                    this.cmLoadSolution2Workspace;
                else
                    set(m, 'Checked', 'off'); 
                    this.vLoad = false; 
                end
            end

        end % Show
        
        function cbSetRefLen(this, varargin)
            minima = zeros(1,length(this.statisticalSets));
            for setCounter = 1:length(this.statisticalSets)
                tmpStatisticalSet = StatisticalSet('Id', this.statisticalSets(setCounter), this.db);
                this.psSet = tmpStatisticalSet.Elements; 
                this.psRef = tmpStatisticalSet.getSeed;
                this.psRefInit;
                sol = this.readNodeSolutions(1);
                minimum = size(sol(1).data,2);       
                for solCounter = 2:length(sol)
                    val = size(sol(solCounter).data,2);
                    if val < minimum
                        minimum = val;
                    end
                end
                minima(setCounter) = minimum;
            end
            refLen = min(minima);
            if nargin > 1
                m = varargin{1};
                if strcmp(get(m, 'Checked'), 'off')
                    set(m, 'Checked', 'on');
                    refLen = floor(refLen/double(this.refStep));
                    this.vGrid = true;
                else
                    set(m, 'Checked', 'off');
                    this.vGrid = false;
                end
            end
            this.refLen = refLen;
            this.refT = linspace(0, 1, this.refLen);
            this.delta = 1.0/(this.refLen-1);
            
            this.SolutionPlot;
            this.HistoPlot;
            if ~isempty(this.hCompare)
                this.ComparePlot;
                this.FeaturePlot;
            end
        end
        
        function cmRemoveTableFilter(this, varargin)
            this.initdata; 
            set(this.hTable, 'Data', this.data);
            this.selection = [];
            this.selection1 = [];
            this.selectionIndices = [];
            
            stop(this.timerCellhDlg); 
            set(this.timerCellhDlg,'TimerFcn',@(x,y)this.nodeclick,'StartDelay',1)
            start(this.timerCellhDlg);  
           
        end
        
        function cmSelectAllTableFilter(this, varargin)
            sz = size(this.data);
            selectionIndices = zeros(sz(1)*sz(2),2);
            sel = []; 

            data = this.data;
            colergen = @(color,text) ['<html><table border=0 width=400 bgcolor=',color,'><TR><TD>',text,'</TD></TR> </table></html>'];
            counter = 1;
            for i = 1:sz(1)
                for j = 1:sz(2)
                    if ~isempty(data{i,j}) 
                        data{i,j} = colergen('#75C0F9',mat2str(data{i,j}));
                        selectionIndices(counter, 1) = i;
                        selectionIndices(counter, 2) = j;
                        counter = counter + 1;
                    end     
                end
            end
            counter = counter - 1;
            this.selectionIndices = selectionIndices;
            if sz(1)*sz(2) ~= counter
                l = counter;
            else
                l = sz(1)*sz(2);
            end
            this.selection = 1:l;
            this.selection1 = 1:l;
            stop(this.timerCellColor); 
            set(this.timerCellColor,'TimerFcn',@(x,y)set(this.hTable,'Data', data),'StartDelay',1)
            start(this.timerCellColor);
            stop(this.timerCellhDlg); 
            set(this.timerCellhDlg,'TimerFcn',@(x,y)this.nodeclick,'StartDelay',1)
            start(this.timerCellhDlg); 
        end
    end
    
    methods(Access = private)
        %% Analyzer Figure
        function cbSaveData(this, varargin)
            [filename,pathname] = uiputfile('*.mat','Select the MATLAB data file');
            if isnumeric(filename)
                if filename == 0
                    return;
                end
            end
            fullname = fullfile(pathname,filename);
            if ~isempty(this.currentNode)
                sol = this.readNodeSolutions(this.currentNode.nodeId);
                data(1).nodeId = this.currentNode.nodeId;
                data(1).solution = sol;
            end
            if ~isempty(this.currentNodeSecondary)
                sol = this.readNodeSolutions(this.currentNodeSecondary.nodeId);
                data(2).nodeId =  this.currentNodeSecondary.nodeId;
                data(2).solution = sol;
            end
            save(fullname,'data');
        end
        
        function cmLoadSolution2Workspace(this, varargin)
            f = waitbar(0,'Please wait...');
            this.loadedSolutions = [];
            currentStatisticalSet = StatisticalSet('Id', this.statisticalSets(1), this.db);   
            this.psSet = currentStatisticalSet.Elements; 
            this.psRef = currentStatisticalSet.getSeed;
            this.psRefInit;
            sol = this.readNodeSolutions(this.currentNode.nodeId);
            this.loadedSolutions = sol;
            cn = 1.0/length(this.statisticalSets);
            waitbar(cn,f,'Loading 1st Statistical Set...');
            if length(this.statisticalSets) > 1
                for setCounter = 2:length(this.statisticalSets)
                    currentStatisticalSet = StatisticalSet('Id', this.statisticalSets(setCounter), this.db);   
                    this.psSet = currentStatisticalSet.Elements; 
                    this.psRef = currentStatisticalSet.getSeed;
                    this.psRefInit;
                    sol = this.readNodeSolutions(this.currentNode.nodeId);
                    this.loadedSolutions(setCounter,:) = sol;
                    if setCounter == 2
                        txt = 'Loading 2nd Statistical Set...';
                    elseif setCounter == 3
                        txt = 'Loading 3rd Statistical Set...';
                    else
                        txt = sprintf('Loading %ith Statistical Set...',setCounter);
                    end
                    cn = (1.0/length(this.statisticalSets))*setCounter;
                    waitbar(cn,f,txt);
                end
            end
            this.psSet = this.currentStatisticalSet.Elements; 
            this.psRef = this.currentStatisticalSet.getSeed;
            this.psRefInit;
            waitbar(1,f,'Finished!');
            pause(1);
            close(f);
        end
        
        function cmVisualize(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'off')
                set(m, 'Checked', 'on'); 
                this.vVisualize = true; 
            else
                set(m, 'Checked', 'off'); 
                this.vVisualize = false; 
            end
            this.Visualize; 
        end
        
        function cmFatness(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'off')
                set(m, 'Checked', 'on'); 
                this.fatness = 2; 
            else
                set(m, 'Checked', 'off'); 
                this.fatness = 1; 
            end
            this.SolutionPlot; 
        end
        
        function cmGradient(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vGradient = false; 
            else
                set(m, 'Checked', 'on')
                this.vGradient = true; 
            end
           this.SolutionPlot; 
        end
             
        function cmPowerSpec(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vPowerSpec = false; 
            else
                set(m, 'Checked', 'on')
                set(this.hcmFFT, 'Checked', 'off')
                set(this.hcmFFTi, 'Checked', 'off')
                set(this.hcmStdDev, 'Checked', 'off')
                this.vStdDev = false;
                this.vPowerSpec  = true; 
                this.vFFT = false; 
                this.vFFTi = false; 
            end
           this.SolutionPlot; 
        end

        function cmFFT (this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vFFT = false; 
            else
                set(m, 'Checked', 'on')
                set(this.hcmPowerSpec, 'Checked', 'off')
                set(this.hcmFFTi, 'Checked', 'off')
                set(this.hcmStdDev, 'Checked', 'off')
                this.vStdDev = false; 
                this.vPowerSpec = false; 
                this.vFFTi = false; 
                this.vFFT = true; 
            end
           this.SolutionPlot; 
        end
        
        function cmFFTi (this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vFFTi = false; 
            else
                set(m, 'Checked', 'on')
                set(this.hcmPowerSpec, 'Checked', 'off')
                set(this.hcmFFT, 'Checked', 'off')
                set(this.hcmStdDev, 'Checked', 'off')
                this.vStdDev = false;
                this.vPowerSpec = false; 
                this.vFFT = false; 
                this.vFFTi = true; 
            end
           this.SolutionPlot; 
        end
        
        function cmPeaks(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vPeaks = false; 
            else
                set(m, 'Checked', 'on')
                set(this.hcmFFT, 'Checked', 'off')
                this.vFFT = false; 
                this.vPeaks = true; 
            end
           this.SolutionPlot; 
        end
        
        function cmLatency(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vLatency = false; 
            else
                set(m, 'Checked', 'on')
                this.vLatency = true; 
            end
           this.SolutionPlot; 
        end

        function cmNormalize(this, varargin)
        %% cmVisualize - contextmenu command callback
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off')
                this.vNormalize = false; 
            else
                set(m, 'Checked', 'on')
                this.vNormalize = true; 
            end
           this.SolutionPlot; 
        end

        function cmValuePlot(this, varargin)
        %% cmValuePlot - contextmenu command callback
            set(this.hcmValuePlot, 'Checked', 'on'); 
            set(this.hcmHistogram, 'Checked', 'off'); 
            this.vValueplot = true; 
            this.HistoPlot; 
        end

        function cmHistogram(this, varargin)
        %% cmSolutionPlot - contextmenu command callback
            set(this.hcmValuePlot, 'Checked', 'off'); 
            set(this.hcmHistogram, 'Checked', 'on'); 
            this.vValueplot = false; 
            this.HistoPlot; 
        end
        
        function cmStdDev(this, varargin)
            m = varargin{1}; 
            if strcmp(get(m, 'Checked'), 'on')
                set(m, 'Checked', 'off');
                set(this.hcmNormalize, 'Enable', 'on');
                this.vStdDev = false;
            else
                set(m, 'Checked', 'on');
                set(this.hcmNormalize, 'Checked', 'on', 'Enable', 'off');
                set(this.hcmPowerSpec, 'Checked', 'off');
                set(this.hcmFFT, 'Checked', 'off');
                this.vNormalize = true;
                this.vStdDev = true;
                this.vPowerSpec = false; 
                this.vFFT = false; 
                this.vFFTi = false; 
            end
           this.SolutionPlot;            
        end
        
        function Visualize(this, psID)
        % Visualize - toggles visualization and synchronizes it with the actual ParamSet
        %   'psID' - optional (default: 1st elem of selection or, if this is emtpy, seed)
        % 
            if this.vVisualize
                if nargin == 1
                    if isempty(this.selection)
                        psID = this.psRef.id; 
                    else
                        psID = this.psSet(this.selection(1)).id;  % 1st element of selection
                    end
                end
                sel = sprintf('select l from NodeParam where paramSetId = %i ORDER BY nodeId', psID); 
                res = this.db.select(sel); 
                if ~isempty(res)
                    for i=2:length(this.arteryNet.nodeArr)
                        this.arteryNet.nodeArr(i).l = res(i-1).l; 
                    end
                else
                    error('Analyser.Visualize: ParamSet with id=%i not found in db', psID); 
                end
                this.arteryNet.root.visualize; 
                if ~isempty(this.currentNode)
                    set(this.currentNode.hNode, 'Visible', 'on'); 
                    set(this.currentNode.hSelect, 'Visible', 'on'); 
                end
                if ~isempty(this.currentNodeSecondary)
                    set(this.currentNodeSecondary.hNode, 'Visible', 'on'); 
                    set(this.currentNodeSecondary.hSelect, 'Visible', 'on'); 
                end
                
            else
                this.arteryNet.root.unvisualize; 
            end
        end
        
        function SolutionPlot(this)
        %% SolutionPlot - plots solutions of selected paramsets for selected in the plotpanel
        %     - modifies plots and titles on this.vNormalize and this.currentNodeSecondary
            set(0, 'CurrentFigure', this.hDlg);
            node = this.currentNode; 
            node1 = this.currentNodeSecondary; 
            set(0, 'CurrentFigure', this.hDlg);
            pos = [0.1 0.15 0.85 0.75]; 
            fUnit = 1; 
            pUnit = 1; 
            c = [1 0 0]; 
            endtime = 0; 
            time = []; 
            % prepare normalized plot
            if node.nodeId == 0
                time = this.psRef.CTS.time;  
                endtime = time(end); 
                cbPlotline = @cbPlotlineRoot; 
            else
                if ~isempty(this.psRef.solution)
                    time = this.psRef.solution.time - this.psRef.solution.time(1); 
                    endtime = time(end); 
                end
                cbPlotline = @cbPlotlineNode; 
            end
            colline = []; 
            plotfp; 
          
            function cbSave(varargin)
            % save plot to clipboard and eps-file on right mouse click
              seltype = get(gcf, 'SelectionType'); 
              if ~strcmp(seltype, 'alt')
                  return; 
              end
               panel = get(varargin{1}, 'Parent'); 
               u = get(panel, 'units'); 
               set(panel, 'units', 'Pixels'); 
               pos = get(panel, 'Position'); 
               set(panel, 'units', u); 
               h1=figure('toolbar','none', 'Position', pos, 'units', u); 
               pos = get(panel, 'Position'); 
               set(panel,'Position', [0 0 1 1]); 
               copyobj(panel,h1); 
               set(panel,'Position', pos); 
               print(h1,'-dbitmap','-painters')
%                [fn path] = uiputfile('*.eps'); 
%                if ~isnumeric(fn)
%                     print(h1,'-depsc2',  '-tiff', '-r600', [path fn]); 
%                end
%             % close(h1);
            end
            
            function plotfp
            % plot flow then pressure
                % retrieve solution data for primary node. first data is seed
                if this.vLoad
                    index = find(this.currentStatisticalSet.getId==this.statisticalSets);
                    sol = this.loadedSolutions(index,:);
                else
                    sol = this.readNodeSolutions(node.nodeId);
                end
                this.hSelectedBins = [];
                t = []; 
                pretext = ''; 
                if this.vNormalize
                    pretext = 'normalized '; 
                end
                if this.vGradient
                    pretext = ['gradient of ' pretext]; 
                end
                if this.vPowerSpec
                    pretext = ['power spectrum of ' pretext]; 
                end
                if this.vFFT
                    pretext = ['FFT real part of ' pretext]; 
                end
                if this.vFFTi
                    pretext = ['FFT imaginary part of ' pretext]; 
                end
                if this.vStdDev
                    pretext = ['standard deviation of ' pretext];
                end
                posttext = sprintf(' [node %i]', node.nodeId); 
                % modify solution if secondary node selected
                if ~isempty(node1)
                    solSecondary = this.readNodeSolutions(node1.nodeId); 
                    for i = 1:length(sol)
                        sol(i).data = sol(i).data-solSecondary(i).data; 
                    end
                    posttext = sprintf(' difference [node %i - node %i]', node.nodeId, node1.nodeId); 
                end
                
                % prepare flow panel 
                subplot(1,1,1, 'Parent', this.hPlotPanelF, 'Position', pos); 
                
                % plot psSet solutions flow
                if ~isempty(sol)  
                    % prepare normalized view
                    if endtime == 0  
                        delta = sol(1).duration/double(size(sol(1).data, 2)); 
                        endtime = sol(1).duration-delta;
                    end
                    if this.vStdDev
                        delta = sol(1).duration/double(size(sol(1).data, 2)); 
                        t = 0:delta: sol(1).duration-delta; 
                        t = t * endtime/t(end); 
                        if length(sol) == 1
                            data = sol(1).data(1,:);
                            if this.vGrid
                                t = t(1:this.refStep:end);
                                data = data(1:this.refStep:end);
                            end
                            plot(t, fUnit * data, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0); 
                            hold on;
                        else
                            %Mean
                            flow = zeros(1,this.refLen);
                            for solCounter = 2:length(sol)
                                flow = flow + this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                            end
                            meanFlow = flow/solCounter;
                            % Standard Deviation
                            stdDevFlow = zeros(1,this.refLen);
                            for solCounter = 2:length(sol)
                                flow = this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                stdDevFlow = stdDevFlow + ((flow-meanFlow).*(flow-meanFlow));
                            end
                            stdDevFlow = sqrt(stdDevFlow/(solCounter-1));
                            fill([t;t],[fUnit * (meanFlow - stdDevFlow);fUnit * (meanFlow + stdDevFlow)], [0.8 0.8 0.8],'Edgecolor', [0.8 0.8 0.8])
                            hold on;
                            plot(t, fUnit * (meanFlow - stdDevFlow), 'b:' ,'Linewidth', this.fatness+1);
                            hold on;
                            plot(t, fUnit * (meanFlow + stdDevFlow), 'b:', 'Linewidth', this.fatness+1);
                            hold on;
                            plot(t, fUnit * meanFlow, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0); 
                            hold on;
                        end                   
                    else    
                        for i=length(sol) :-1:1
                            delta = sol(i).duration/double(size(sol(i).data, 2)); 
                            t = 0:delta: sol(i).duration-delta; 
                            if this.vNormalize
                                t = t * endtime/t(end); 
                            end
                            data = operators(sol(i).data(1,:)); %#ok<*PROP>
                            if this.vGrid
                                t = t(1:this.refStep:end);
                                data = data(1:this.refStep:end);
                            end
                            if i == 1  % first solution is seed!
                                if this.vPowerSpec
                                   semilogy(t, fUnit * data, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline); 
                                else
                                   plot(t, fUnit * data, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline); 
                                end
                            else
                                if this.vPowerSpec
                                   semilogy(t, fUnit * data, 'Linewidth', this.fatness,'UserData', this.selection(i-1), 'ButtonDownFcn', cbPlotline); 
                                else
                                   plot(t, fUnit * data, 'Linewidth', this.fatness, 'UserData', this.selection(i-1), 'ButtonDownFcn', cbPlotline); 
                                end
                            end
                            hold on; 
                        end
                    end
                end
                
                % plot psRef solution flow if only one node is selected
                if node.nodeId == 0
%                     data = this.psRef.CTS.data(1,:);  
%                     t = this.psRef.CTS.time; 
%                     plot(t, pUnit* operators(data), 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline);  
%                 elseif ~isempty(this.psRef.solution) && isempty(node1)
%                     data = this.psRef.solution.Y(:,2*node.nodeId-1);   % flow has odd indices
%                     delta = this.psRef.solution.duration/double(size(data, 1)); 
%                     t = 0:delta: this.psRef.solution.duration-delta; 
%                     plot(t, pUnit* operators(data), 'Linewidth', 3, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline);  
                end
                % labels
                title([pretext 'flow' posttext], 'Fontsize', 12); 
                if this.vPowerSpec || this.vFFT || this.vFFTi
                    xlabel(gca, 'freq [Hz]'); 
                else
                    xlabel(gca, 'time [s]'); 
                    ylabel(gca, 'flow [ml/s]'); 
                end
                hlbl = get(gca, 'XLabel'); 
                set(hlbl, 'ButtonDownFcn', @toggleNormalize); 
                set(gca, 'ButtonDownFcn', @cbSave); 
                

                % prepare pressure panel 
                subplot(1,1,1, 'Parent', this.hPlotPanelP, 'Position', pos); 
                
                % plot psSet solutions pressure
                if ~isempty(sol)
                    if this.vStdDev
                        delta = sol(1).duration/double(size(sol(1).data, 2)); 
                        t = 0:delta: sol(1).duration-delta; 
                        t = t * endtime/t(end); 
                        if length(sol) == 1
                            data = sol(1).data(1,:);
                            if this.vGrid
                                t = t(1:this.refStep:end);
                                data = data(1:this.refStep:end);
                            end
                            plot(t, pUnit * data, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0); 
                            hold on;
                        else
                            %Mean
                            pressure = zeros(1,this.refLen);
                            for solCounter = 2:length(sol)
                                pressure = pressure + this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                            end
                            meanPressure = pressure/solCounter;
                            % Standard Deviation
                            stdDevPressure = zeros(1,this.refLen);
                            for solCounter = 2:length(sol)
                                pressure = this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                                stdDevPressure = stdDevPressure + ((pressure-meanPressure).*(pressure-meanPressure));
                            end
                            stdDevPressure = sqrt(stdDevPressure/(solCounter-1));
                            fill([t;t],[pUnit * (meanPressure - stdDevPressure);pUnit * (meanPressure + stdDevPressure)],  [0.8 0.8 0.8],'Edgecolor', [0.8 0.8 0.8])
                            hold on;
                            plot(t, pUnit * (meanPressure - stdDevPressure), 'b:' ,'Linewidth', this.fatness+1);
                            hold on;
                            plot(t, pUnit * (meanPressure + stdDevPressure), 'b:', 'Linewidth', this.fatness+1);
                            hold on;
                            plot(t, pUnit * meanPressure, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0); 
                            hold on;
                        end
                    else
                        for i=length(sol) :-1:1
                            delta = sol(i).duration/double(size(sol(i).data, 2)); 
                            t = 0:delta: sol(i).duration-delta; 
                            if this.vNormalize
                                t = t * endtime/t(end); 
                            end
                            data = operators(sol(i).data(2,:));
                            if this.vGrid
                                t = t(1:this.refStep:end);
                                data = data(1:this.refStep:end);
                            end
                            if i == 1
                                if this.vPowerSpec
                                    semilogy(t, pUnit * data, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0,  'ButtonDownFcn', cbPlotline); 
                                else
                                    plot(t, pUnit * data, 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline); 
                                end
                            else
                                if this.vPowerSpec
                                    semilogy(t, pUnit * data, 'Linewidth', this.fatness, 'UserData', this.selection(i-1), 'ButtonDownFcn', cbPlotline); 
                                else
                                    plot(t, pUnit * data, 'Linewidth', this.fatness, 'UserData', this.selection(i-1), 'ButtonDownFcn', cbPlotline); 
                                end
                            end
                            hold on; 
                        end
                    end
                end
                
                % plot psRef solution pressure  if only one node is selected
                if node.nodeId == 0
%                     data = this.psRef.CTS.data(2,:);  
%                     t = this.psRef.CTS.time; 
%                     plot(t, pUnit* operators(data), 'Linewidth', 1+this.fatness, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline);  
%                 elseif ~isempty(this.psRef.solution) && isempty(node1)
%                     data = this.psRef.solution.Y(:,2*node.nodeId);   % pressure has even indices
%                     delta = this.psRef.solution.duration/double(size(data, 1)); 
%                     t = 0:delta: this.psRef.solution.duration-delta; 
%                     plot(t, pUnit* operators(data), 'Linewidth', 3, 'Color', c, 'UserData', 0, 'ButtonDownFcn', cbPlotline);  
                end
                % labels
                title([pretext 'pressure' posttext], 'Fontsize', 12); 
                if this.vPowerSpec || this.vFFT || this.vFFTi
                    xlabel(gca, 'freq [Hz]'); 
                else
                    xlabel(gca, 'time [s]'); 
                    if pUnit == 1
                        ylabel(gca, 'pressure [Pa]'); 
                    else
                        ylabel(gca, 'pressure [mmHg]'); 
                    end
                end
                hlbl = get(gca, 'YLabel'); 
                set(hlbl, 'ButtonDownFcn', @toggleUnit); 
                hlbl = get(gca, 'XLabel'); 
                set(hlbl, 'ButtonDownFcn', @toggleNormalize); 
                set(gca, 'ButtonDownFcn', @cbSave); 
                
                delete(this.hPropText); 
                delete(this.hPropVal); 
                this.hPropText = []; 
                this.hPropVal = []; 
                colline = []; 
                
                function data = operators(data)
                    if this.vGradient
                        data = gradient(data);
                    end
                    if this.vFFT
                        pUnit = 1; 
                        data = fftcoeff(data); 
                    end
                    if this.vFFTi
                        pUnit = 1; 
                        data = fftcoeffi(data); 
                    end
                    if this.vPowerSpec
                        pUnit = 1; 
                        data = powerspec(data); 
                    end
               end
                
                function dta = powerspec(dat)
                    N = length(t); 
                    dta = abs(fft(dat))/N*2;   % absolute value of the fft
                    dta = dta(1:ceil(N/2)).^2; 
                    t = (0:ceil(N/2)-1)/t(end); %#ok<COLND> % find the corresponding frequency in Hz
                end
                
                function dta = fftcoeff(dat)
                    N = length(t); 
                    dta = real(fft(dat))/N*2;   % absolute value of the fft
                    dta = dta(1:ceil(N/2)); 
                    t = (0:ceil(N/2)-1)/t(end); %#ok<COLND> % find the corresponding frequency in Hz
                end
                function dta = fftcoeffi(dat)
                    N = length(t); 
                    dta = imag(fft(dat))/N*2;   % absolute value of the fft
                    dta = dta(1:ceil(N/2)); 
                    t = (0:ceil(N/2)-1)/t(end); %#ok<COLND> % find the corresponding frequency in Hz
                end               
            end  % plotfp
            
            function id = prepareColoringTextVisulize(idx)
                if ~isempty(colline)
                    w = get(colline, 'LineWidth');
                    if w{1} == 3  
                        set(colline, 'Color', 'r');  
                    else
                        set(colline, 'Color', 'b');  
                    end
                end
                % selection gets green color
                colline = findobj(this.hDlg, 'Type', 'line', 'Userdata', idx); 
                set(colline, 'Color', [0 1 0]);  
                delete(this.hPropText); 
                delete(this.hPropVal); 
                this.hPropText = []; 
                this.hPropVal = []; 
                if ~idx   % psRef
                    id = this.psRef.id; 
                else
                    id = this.psSet(idx).id; 
                end
                this.Visualize(id); 
            end
            
            function cbPlotlineRoot(varargin)
            %% cbPlotline - selects plotline of corresponding ParamSet in both panels, updates value plot and visualization
            %     and shows root node parameters in the property area of the main window
                idx = get(varargin{1}, 'UserData');  % index of Element in StatisticalSet
                id = prepareColoringTextVisulize(idx); 

                sel = sprintf('select typeName, name, eta, rho, gamma, heartRate from NetParam as np join NetType as nt on np.type = nt.id where paramSetId = %i', id); 
                res = this.db.select(sel); 
                res1 = this.db.select(['select name from ParamSet where id = ' num2str(id)]);  

                fields = sprintf('PSid: \nnode: \ntype: \nname: \n\\eta: \n\\rho: \n\\gamma: \nHR: \n \n \n \n \n '); 
                vals = sprintf('%i %s\n%i\n%s\n%s\n%.2e\n%.2e\n%.2e\n%.2e \n \n \n \n \n ', ...
                    id, res1.name, this.currentNode.nodeId, res.typeName, res.name, res.eta, res.rho, res.gamma, res.heartRate); 

                % calc to bring text to top left corner
                this.hPropText = text(0, 0, fields, 'Units', 'pixels', 'HorizontalAlignment', 'left', 'Parent', this.hAxes); 
                this.hPropVal = text(0, 0, vals, 'Units', 'pixels', 'HorizontalAlignment', 'left', 'Parent', this.hAxes); 
                this.placeProps; % get values and show text fields in main window part

                % synchronize HistoPlot/ValuePlot
                if this.vValueplot && idx
                    for j=1: length(this.selection)
                        if this.selection(j) == idx
                            this.SelectValuePlotBins(j);
                            break; 
                        end
                    end
                end
                % synchronize Visualize

            end           
            function cbPlotlineNode(varargin)
             %% cbPlotlineNode - selects plotline of corresponding ParamSet in both panels, updates value plot and visualization
             %     and shows node parameters in the property area of the main window
                idx = get(varargin{1}, 'UserData');  % index of Element in StatisticalSet
                id = prepareColoringTextVisulize(idx); 
                
                sel = sprintf('select eta, rho, typeName, np.name, E, l, h, d from NodeParam as np join NodeType as nt on np.type = nt.id join NetParam as ne on ne.paramsetId = np.paramsetId where np.paramSetId = %i and nodeId = %i', id, this.currentNode.nodeId); 
                res = this.db.select(sel); 
                res1 = this.db.select(['select name from ParamSet where id = ' num2str(id)]);  
                res = ArteryNet.Elhd2RCL(res); 
                sel = sprintf('select  R, C, L, p, q from BoundaryParam where paramSetId = %i and nodeId = %i', id, this.currentNode.nodeId); 
                resb = this.db.select(sel); 

                fields = sprintf('PSid: \nnode: \ntype: \nname: \nE: \nl: \nh: \nd: \nR: \nC: \nL: \nAo: \nco: '); 
                vals = sprintf('%i %s\n%i\n%s\n%s\n%.2e\n%.2e\n%.2e\n%.2e\n%.2e\n%.2e\n%.2e\n%.2e\n%.2e', ...
                    id, res1.name, this.currentNode.nodeId, res.typeName, res.name, res.E, res.l, res.h, res.d, res.R, res.C, res.L, res.Ao, res.co); 
                if ~isempty(resb)
                    fields = sprintf('%s\n-----------\nR: \nC \nL: \np: \nq: ', fields); 
                    vals = sprintf('%s\n---------------\n%.2e\n%.2e\n%.2e\n%.2e\n%.2e', vals, resb.R, resb.C,  resb.L,  resb.p,  resb.q);   
                end

                % calc to bring text to top left corner
                this.hPropText = text(0, 0, fields, 'Units', 'pixels', 'HorizontalAlignment', 'left', 'Parent', this.hAxes); 
                this.hPropVal = text(0, 0, vals, 'Units', 'pixels', 'HorizontalAlignment', 'left', 'Parent', this.hAxes); 
                this.placeProps; 

                % synchronize ValuePlot

                if this.vValueplot && idx
                    for j=1: length(this.selection)
                        if this.selection(j) == idx
                            this.SelectValuePlotBins(j);
                        end
                    end
                end

            end           
            function toggleNormalize(varargin)
                this.vNormalize = ~this.vNormalize; 
                plotfp; 
            end

            function toggleUnit(varargin)
                if pUnit == 1
                    pUnit = 7.5E-3; 
                else
                    pUnit = 1; 
                end
                plotfp; 
            end

        end % SolutionPlot
        
        function placeProps(this)
            if isempty(this.hPropText)
                return; 
            end
            eT = get(this.hPropText, 'Extent');
            eV = get(this.hPropVal, 'Extent');
            units = get(this.hAxes, 'Units'); 
            set(this.hAxes, 'units', 'pixels'); 
            axPos = get(this.hAxes, 'Position'); 
            set(this.hAxes, 'units', units); 
            set(this.hPropText, 'Position', [5 axPos(4)+eT(2)]); 
            set(this.hPropVal, 'Position', [40 axPos(4)+eV(2)]); 
        end
       
        function SelectValuePlotBins(this, j)
            % calculate the fills in all plots
            set(0, 'CurrentFigure', this.hDlg);
            if ~isempty(this.hSelectedBins);
                delete(this.hSelectedBins);
                this.hSelectedBins = [];
            end
            Patches = findobj(this.hHistoPanel,'Type','patch');
            Axes = findobj(this.hHistoPanel,'Type','Axes');
            for ii=1: length(Patches)
                p_x = get(Patches(ii), 'Xdata');
                p_y = get(Patches(ii), 'Ydata');
                x = p_x(:, j);
                y = p_y(:,j);
                axes(Axes(ii));
                hold all;
                this.hSelectedBins(ii) = fill(x, y, 'k');
            end
        end
    
        function circleHistogram(this, varargin)
            this.vHistomode = mod(this.vHistomode+1, 4); 
            this.HistoPlot; 
        end
        
        function cmChangeStatisticalSet(this)
            set(0, 'CurrentFigure', this.hDlg);
            hCurrentStatisticalSetButton = findall(gcf, 'Type', 'uicontrol', 'FontWeight', 'bold');
            set(hCurrentStatisticalSetButton, 'FontWeight', 'Normal','BackgroundColor', [0.94 0.94 0.94]);
            this.currentStatisticalSet = StatisticalSet('Id', this.firstSet, this.db);
            hCurrentStatisticalSetButton = findall(gcf, 'Type', 'uicontrol', 'String', string(this.firstSet));
            set(hCurrentStatisticalSetButton, 'FontWeight', 'Bold','BackgroundColor', [0.65 0.65 0.65]);
            nNodes = length(this.arteryNet.adjacency)-1;
            this.arteryNet = this.currentStatisticalSet.arteryNet; 
            this.psSet = this.currentStatisticalSet.Elements; 
            this.psRef = this.currentStatisticalSet.getSeed;
            this.psRefInit;
            obj = this.arteryNet; 
            cla(this.hAxes)
            this.hAxes = axes('Parent', this.hNetPanel, 'Xlim', obj.axesRect(1:2)*0.9, 'Ylim', obj.axesRect(3:4)*0.9, 'XTick', [], 'YTick', [], ...
                'Position', [0 0 1 1]);
            this.arteryNet.root.add2Dlg(this.hNetPanel, @this.nodeclick);   % populate with nodes & register callback for mouseclick
            set(obj.root.hSelectText, 'ButtonDownFcn', @this.nodeclick); 
            set(obj.root.hSelect, 'ButtonDownFcn', @this.nodeclick); 
            % prepare Paramset selector table
            this.initdata; 
            this.ids = [this.psSet.id]; 
            data = this.data;
            colergen = @(color,text) ['<html><table border=0 width=400 bgcolor=',color,'><TR><TD>',text,'</TD></TR> </table></html>'];
            for i = 1:length(this.selectionIndices)
                index = this.selectionIndices(i,:);
                data{index(1),index(2)} = colergen('#75C0F9',mat2str(data{index(1),index(2)}));
            end
            set(this.hTable,'Data', data); 
            if nNodes > length(this.arteryNet.adjacency)-1
                this.currentNode = this.arteryNet.nodeArr(1);
            else
                this.currentNode = this.arteryNet.nodeArr(this.currentNode.nodeId+1);
            end
            this.loading = false;
            this.nodeclick;
        end
        
        function nodeclick(this, varargin) 
        % nodeclick - callback registered for 
        %      - each node's rect and text UI elements
        %      - timer on selection changes in table
            obj = this.arteryNet;
            st = get(this.hDlg, 'SelectionType'); 
            if nargin > 1 && ~isa(varargin{1}, 'Node')
                this.currentNode = obj.root;
                this.currentNodeSecondary = []; 
            else
                if nargin > 1 && ~strcmp(st, 'extend')
                    this.currentNode = varargin{1}; 
                    this.currentNodeSecondary = []; 
                end
                if nargin > 1 && strcmp(st, 'extend') && this.currentNode ~= obj.root
                    this.currentNodeSecondary = varargin{1}; 
                end
             end

            if isempty(this.currentNode)
                return; 
            end
            rng('default'); %#ok<RAND> MANUEL: neue Matlab Version hat die jetzige Syntax empfohlen, davor: rand('seed',102); 

            for j=1:length(obj.nodeArr) % unselect all nodes
                obj.nodeArr(j).Select(false); 
                if this.vVisualize && j>1
                    set(obj.nodeArr(j).hNode, 'Visible', 'off'); 
                    set(obj.nodeArr(j).hSelect, 'Visible', 'off'); 
                end
            end
            this.currentNode.Select(true); 
            if this.vVisualize
                this.Visualize; 
                set(this.currentNode.hNode, 'Visible', 'on'); 
                set(this.currentNode.hSelect, 'Visible', 'on'); 
            end
            if ~isempty(this.currentNodeSecondary)
                this.currentNodeSecondary.Select(true); 
                if this.vVisualize
                    set(this.currentNodeSecondary.hNode, 'Visible', 'on'); 
                    set(this.currentNodeSecondary.hSelect, 'Visible', 'on'); 
                end
            end
            if this.vLoad && this.loading
                this.cmLoadSolution2Workspace;
            else
                this.loading = true;
            end
            this.HistoPlot;
            this.SolutionPlot;
            if ~isempty(this.hCompare)
                this.ComparePlot;
            end
        end

        function HistoPlot(this)
        % plots histograms and value bar diagrams of node values into hHistoPanel
        %  - distinguishes between netParam from nodeParam values
        %  - sets a callback for all bins which marks the selected bin 
        %    and sets a selection filter for the table 
            set(0, 'CurrentFigure', this.hDlg);
            node = this.currentNode; 
            %prepare subplots
            set(0, 'CurrentFigure', this.hDlg);
            pos1 = [0.03 0.25 0 0.7];  % pos of first subplot
            cols = [0.8 0.8 .3; 1 0.5 0.5; 0.5 1 0.5; 0.5 0.5 1; 0.3 0.8 .8; 0.8 0.3 .8]; % 6 colors
            delete(findobj(this.hHistoPanel, 'Type', 'axes')); 
            
            %Select between Histo- and Value-Plot
            if this.vValueplot
                subplots = @subplotsValue;
                % prepare where clause for db.select
                if isempty(this.selection)
                    return; 
                end
                sel = sprintf('n.paramSetId = %i', this.psSet(this.selection(1)).id); 
                for i = 2:length(this.selection)
                    sel = sprintf('%s or n.paramSetId = %i', sel, this.psSet(this.selection(i)).id); 
                end
            else
                subplots = @subplotsHisto;
                % prepare where clause for db.select
                sel = sprintf('n.paramSetId = %i', this.psSet(1).id); 
                for i = 2:length(this.psSet)
                    sel = sprintf('%s or n.paramSetId = %i', sel, this.psSet(i).id); 
                end
            end
            
            % get values for this node from database and calculate R, C, L, Ao, co
            
            if node.nodeId == 0
                % get values for this node from database 
                sel = sprintf('select heartRate, eta, rho, gamma from NetParam as n where %s', sel); 
                res = this.db.select(sel); 
                subplots({'heartRate' 'eta' 'rho' 'gamma' }); 
            else
                if node.isBoundary
                    sel = sprintf('select E, n.l, h, d, eta, rho, R as R_b, C as C_b, BoundaryParam.L as L_b, p as p_b, q as q_b from NodeParam as n join NetParam on n.paramSetId = NetParam.paramSetId join BoundaryParam on BoundaryParam.paramSetId = n.paramSetId where n.nodeId = BoundaryParam.nodeId and n.nodeId = %i and (%s)', node.nodeId, sel); 
                else
                    sel = sprintf('select E, l, h, d, eta, rho from NodeParam as n join NetParam on n.paramSetId = NetParam.paramSetId where nodeId = %i and (%s)', node.nodeId, sel); 
                    this.vHistomode = mod(this.vHistomode, 3); 
                end
                res = this.db.select(sel); 
                res = ArteryNet.Elhd2RCL(res); 
                switch this.vHistomode
                    case 0
                        subplots({'E' 'l' 'h' 'd' }); 
                    case 1
                        subplots({'R' 'C' 'L'}); 
                    case 2
                        subplots({'Ao' 'co' });  %  'Z' missing
                    case 3
                        fields = {'R_b' 'C_b' 'L_b' 'p_b' 'q_b'};
                        % exclude empty fields
                        j = 0; 
                        for i= 1:length(fields)
                            if ~isempty([res.(fields{i})])
                                j = j+1; 
                                fie{j} = fields{i}; 
                            end
                        end
                        subplots(fie); 
                end
                
            end
            hSelected = []; 

            function subplotsValue(fields) 
            % plots 1 - 6 plots into hHistoPanel
            %   'fields' - cell array of fieldnames to be plotted
                len = length(fields);
                for i=1:len
                    f = fields{i}; 
                    p = pos1 + [(i-1)*1./len 0 1./((1+0.1*len)*len) 0]; 
                    subplot(1, len, i, 'Parent', this.hHistoPanel, 'Position', p); 
                    if isfield(this.psRef.netParam, f)
                        bar([res.(f)] - this.psRef.netParam.(f)); 
                        labl = sprintf('%s _{[%.2e]}', f, this.psRef.netParam.(f)); 
                    else
                        bar([res.(f)] - this.psRef.nodeParam(node.nodeId).(f)); 
                        labl = sprintf('%s _{[%.2e]}', f, this.psRef.nodeParam(node.nodeId).(f)); 
                    end
                    h = findobj(gca,'Type','patch');
                    set(h,'FaceColor',cols(i,:),'EdgeColor','k', 'HitTest', 'on', 'ButtonDownFcn', @cbValueSelectBin);
                    xlabel(gca, labl); 
                end
            end
            
            function cbValueSelectBin(varargin)
            % cbPatch - generic callback registered for Patch objects
            %    selects a bin by inserting a fill at the right position; 
            %    
               pos = get(gca, 'CurrentPoint'); 
               hPatch = findobj(gca,'Type','patch')  ; 

               % find the cluster bin, store as j
                p_x = get(hPatch (end), 'Xdata'); 
                for j = 1:size(p_x,2)-1; 
                   if pos(1) >= p_x(1,j) && pos(1) < p_x(2,j+1)
                       break; 
                   end
                end
                if pos(1) > p_x(2,j+1)
                    j = j+1; 
                end
                % find the corresponding line object in the solution plot 
                id = this.selection(j); 
                
                % tricky code: we trigger the ButtonDownFcn of the line to 
                %   - mark the lines in both plots
                %   - output property text 
                %   - mark the ValuePlot bins !!
                %   - update visualization
                line = findobj(this.hPlotPanelF, 'Type', 'line', 'Userdata', id); 
                fn  = get(line, 'ButtonDownFcn'); 
                if ~isempty(fn)
                    feval(fn, line);    % cbPlotlineXXX
                end
            end
            function subplotsHisto(fields)
            % plots 1 - 6 plots into hHistoPanel
            %   'fields' - cell array of fieldnames to be plotted
                len = length(fields);
                for i=1:len
                    f = fields{i}; 
                    p = pos1 + [(i-1)*1./len 0 1./((1+0.1*len)*len) 0]; 
                    subplot(1, len, i, 'Parent', this.hHistoPanel, 'Position', p); 
                    hist([res.(f)]); 
                    h = findobj(gca,'Type','patch');
                    set(h,'FaceColor',cols(i,:),'EdgeColor','k', 'ButtonDownFcn', @cbHistoSelectBin, 'UserData', calcHistSelections(f) );
                    if isfield(this.psRef.netParam, f)
                        labl = sprintf('%s _{[%.2e]}', f, this.psRef.netParam.(f)); 
                    else
                        labl = sprintf('%s _{[%.2e]}', f, this.psRef.nodeParam(node.nodeId).(f)); 
                    end
                    xlabel(gca, labl); 
                end
            end
            function clustering = calcHistSelections(f)
                [hi ha] = hist([res.(f)]);
                delta = (ha(2)-ha(1))/2; 
                len = length(hi); 
                clustering(length(res)) = 0; 
                % last bin
                for k = 1: length(res)
                    try
                        if res(k).(f) > ha(end) - delta
                            clustering(k) = len; 
                        end
                    catch
                        clustering(k) = 1; 
                    end
                end
                % middle bins
                for o = 2: len-1
                    for k = 1: length(res)
                        try
                            if res(k).(f) > ha(o)-delta && res(k).(f) <= ha(o)+delta
                                clustering(k) = o; 
                            end
                        catch
                            clustering(k) = 1; 
                        end
                    end
                end
                % first bin
                for k = 1: length(res)
                    try
                        if res(k).(f) <= ha(1)+ delta
                            clustering(k) = 1; 
                        end
                    catch
                        clustering(k) = 1; 
                    end
                end
            end
            function cbHistoSelectBin(varargin)
            % cbPatch - generic callback registered for Patch objects
            %    selects a bin by inserting a fill at the right position; 
            %    
               pos = get(gca, 'CurrentPoint'); 
               if ~isempty(hSelected); 
                   delete(hSelected); 
                   hSelected = []; 
               end
               hPatch = findobj(gca,'Type','patch');  
               
               % find the cluster bin j
                p_x = get(hPatch (end), 'Xdata'); 
                for j = 1:size(p_x,2)-1; 
                   if pos(1) >= p_x(1,j) && pos(1) < p_x(2,j+1)
                       break; 
                   end
                end
                if pos(1) > p_x(2,j+1)
                    j = j+1; 
                end
                
                % calculate the fill 
                p_y = get(hPatch(end), 'Ydata'); 
                x = p_x(:, j); 
                y = p_y(:,j); 
                hold all; 
                hSelected = fill(x, y, 'k', 'ButtonDownFcn', @cbUnSelectBin); 
                
                % calculate the selection and plot it
                clustering = get(hPatch, 'UserData'); 
                this.selection = find(clustering == j);  
                this.SolutionPlot; 
                this.initdata([this.psSet(this.selection).id]); 
                set(this.hTable, 'Data', this.data); 
                
                % remove table selections; 
%                 if ~isempty(this.selection1)
%                     set(this.hTable, 'Data', []); 
%                     set(this.hTable, 'Data', this.data); 
%                     this.selection1 = []; 
%                 end
                
            end
            function cbUnSelectBin(varargin)
               if ~isempty(hSelected); 
                   delete(hSelected); 
                   hSelected = []; 
               end
               % unrestrict the table; 
               this.cmRemoveTableFilter; 
            end
            
        end % HistoPlot
        
        function initdata(this, idlist)
        %  'idlist' - optional (default = all ids)
        %
            this.data = []; 
            if nargin < 2
                rows = ceil(length(this.psSet)/this.colsPerRow); 
                this.data{this.colsPerRow, rows} = ''; 
                for i=1:length(this.psSet)
                    this.data{i} = this.psSet(i).id; 
                end
            else
                rows = ceil(length(idlist)/this.colsPerRow); 
                this.data{this.colsPerRow, rows} = ''; 
                for i=1:length(idlist)
                    this.data{i} = idlist(i); 
                end
            end
            this.data = this.data'; 
            
        end
        
        function sol = readNodeSolutions(this, nodeId)
        %% readNodeSolutions - reads single node solutions for a set of ParamSets in a speed optimized way directly from db
        %    PARAMS
        %       'nodeId'   - identifies the node
        %       'psIdList' - list of ParamSet Ids, if empty, 'sol' will be empty
        %       'sol'      - resulting solution-Array
            
            % get values for this node from database 
            sol = []; 
            
            if nodeId == -1
                where = sprintf('paramSetId = %i', this.psRef.id); 
                for i = 1: length(this.selection)
                    where = sprintf('%s OR paramSetId = %i', where, this.psSet(this.selection(i)).id); 
                end
                sel = sprintf('SELECT paramSetId, heartRate, unit, rate, data FROM NetParam join CTS on inboundCtsId = CTS.id WHERE %s ORDER BY paramSetId', where); 
                sol = this.db.select(sel); 
                for i=1:length(sol)
                    data2 = typecast(sol(i).data', 'double');
                    data1 = zeros(1,length(data2));  % flow
                    sol(i).data = [data1; data2]; 
                    sol(i).duration = 60./ double(sol(i).heartRate);
                end
            else
                % construct sql select statement
                where = sprintf('solverRunId = %i', this.psRef.currentSolverRun); 
                for i = 1: length(this.selection)
                    where = sprintf('%s OR solverRunId = %i', where, this.psSet(this.selection(i)).currentSolverRun);
                end
                sel = sprintf('SELECT STS.rate, data, paramSetId FROM STS JOIN SolverRun ON solverRunId = SolverRun.id WHERE nodeId = %i and (%s) ORDER BY paramSetId', nodeId+1, where); 

                % get result set and
                sol = this.db.select(sel); 
                if isempty(sol)
                    error('Analyzer.readNodeSolutions: unexpected empty result set for sql command\n%s\n', sel); 
                end
                for i=1:length(sol)
                    pq = int16(length(sol(i).data)/16); % # samples [p q]
                    data = typecast(sol(i).data', 'double'); %#ok<PROP>
                    sol(i).data = reshape(data, pq, 2)'; %#ok<PROP>
                    sol(i).duration = double(pq)/double(sol(i).rate);
                end
            end
        end % readNodeSolutions
        
        %% Compare Figure
        function ShowCompare(this, varargin)
            this.timerCellhCompare = timer;
            scrsz = get(0,'ScreenSize');
            mp = get(0, 'MonitorPositions'); 
            if size(mp,1) > 1
                scrf = 1.017 * scrsz(3); 
            else
                scrf = 0.017 * scrsz(3); 
            end
            pos = [scrf scrsz(4)/44  16*scrsz(4)/11   10*scrsz(4)/11];
            if isempty (this.hCompare) || ~ishandle(this.hCompare)
                this.hCompare = figure('Name', 'Statistical Set compare window', 'NumberTitle', 'off', 'WindowStyle', 'normal', 'MenuBar','none', 'ToolBar','none', ...
                     'Position',pos, 'ToolBar','none', 'DeleteFcn', @this.cbDelCompare);
            else
                guiobj = findobj(this.hCompare); 
                delete(guiobj(2:end));
            end
            
            this.hFileMenuCompare = uimenu(this.hCompare, 'Label', 'File');
            uimenu(this.hFileMenuCompare, 'Label', 'Save last clicked Compare plot to *.png','Callback', @cbSaveCurrentPlot2Png);
            uimenu(this.hFileMenuCompare, 'Label', 'Save Feature plot to *.png','Callback', @cbSaveFeaturePlot2Png);
            
            this.hClassifierMenu = uimenu(this.hCompare, 'Label', 'Classifier');
            uimenu(this.hClassifierMenu, 'Label', 'Open new Classifier', 'Callback', @cbNewClassifierObj);
            uimenu(this.hClassifierMenu, 'Label', 'Load Classifier', 'Callback', @cbLoadClassifierObj);
            this.hcmDistinguishDiagnoses = uimenu(this.hClassifierMenu, 'Separator', 'on', 'Label', 'Distinguish between diagnoses', 'Checked', 'on', 'Enable', 'off','Callback', @cmDistinguishBetween);
            this.hcmDistinguishSets = uimenu(this.hClassifierMenu, 'Label', 'Distinguish between Statistical Sets', 'Checked', 'off', 'Callback', @cmDistinguishBetween);
            
            this.hFeatureViewMenu = uimenu(this.hCompare, 'Label', 'Views feature plot');
            this.hcmActualDistribution = uimenu(this.hFeatureViewMenu, 'Label', 'Actual distribution', 'Checked', 'off', 'Enable', 'on', 'Callback', @cmActualDistribution);
            this.hcmGaussianDistribution = uimenu(this.hFeatureViewMenu, 'Label', 'Gaussian distribution', 'Checked', 'off', 'Enable', 'on', 'Callback', @cmGaussianDistribution);
            uimenu(this.hFeatureViewMenu,'Separator', 'on', 'Label', 'View means', 'Checked', 'off', 'Enable', 'on', 'Callback', @cbViewMeans);
            uimenu(this.hFeatureViewMenu, 'Label', 'View deviations', 'Checked', 'off', 'Enable', 'on', 'Callback', @cbViewDeviations);
           
            this.hCompareViewMenu = uimenu(this.hCompare, 'Label', 'Views compare plots');
            uimenu(this.hCompareViewMenu, 'Label', 'Link brush left/right', 'Checked', 'off', 'Callback', @cmLinkBrushSelectionLR);
            uimenu(this.hCompareViewMenu, 'Label', 'Link brush up/down', 'Checked', 'off', 'Callback', @cmLinkBrushSelectionUD);
            uimenu(this.hCompareViewMenu, 'Separator', 'on', 'Label', 'Gradient', 'Checked', 'off', 'Callback', @cmGradient);
            this.hcmFFTCompare = uimenu(this.hCompareViewMenu, 'Label', 'Amplitude spectrum', 'Checked', 'off', 'Callback', @cmAmplitudeSpec);
            this.hcmFFTiCompare = uimenu(this.hCompareViewMenu, 'Label', 'Phase spectrum', 'Checked', 'off', 'Callback', @cmPhaseSpec); 
            
            this.hCovMatPanel = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0 0.66 0.3 0.34]);
            this.hCovPanelF = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0 0.33 0.5 0.33]);
            this.hCovPanelP = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0 0    0.5 0.33]);
            this.hStdDevPanelF = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0.5 0.33 0.5 0.33]);
            this.hStdDevPanelP = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0.5 0    0.5 0.33]);
            this.hControlPanel = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0.3 0.66  0.1 0.34]);
            this.hFeaturePanel = uipanel('Parent', this.hCompare, 'Title', '',  'Units', 'normalized', 'Position', [0.4 0.66  0.6 0.34]);
            
            data = cell(length(unique(this.diagnoses.id)));
            data(:,:) = {'COV'};
            for i = 1:length(unique(this.diagnoses.id))
                data(i,i) = {strcat(char(963),char(178))};
            end
            [columnWidth{1:length(unique(this.diagnoses.id))}] = deal(40);
            this.CovTableData = data;
            this.hCovTable = uitable(this.hCovMatPanel, 'Units', 'normalized', 'Position', [0 0 1 1], 'CellSelectionCallback', @cbCovMatSelection, ...
                'ColumnName', unique(this.diagnoses.name), 'RowName', unique(this.diagnoses.name),...
                'Data', data, 'ColumnWidth', columnWidth);
            
            this.hXAxisDropDown = uicontrol('Style','popupmenu','Parent', this.hControlPanel,'String', {''},'Units', 'normalized', 'Position', [0 0.75 1 0.1], 'Callback', @cbDropDownButton);
            this.hYAxisDropDown = uicontrol('Style','popupmenu','Parent', this.hControlPanel,'String',{''},'Units', 'normalized', 'Position', [0 0.55 1 0.1], 'Callback', @cbDropDownButton);
            uicontrol('Style','text','Parent',this.hControlPanel, 'FontWeight','bold','Units','normalized','String','X Axis', 'Position',[0 0.87 1 0.05]);
            uicontrol('Style','text','Parent',this.hControlPanel, 'FontWeight','bold','Units','normalized','String','Y Axis', 'Position',[0 0.67 1 0.05]);
            this.hFeatureAddButton = uicontrol('Style','pushbutton','Parent', this.hControlPanel,'String','Add',  'Units', 'normalized', 'Position', [0 0.3 1 0.1], 'Enable', 'off', 'Callback', @cbFeatureAddButton);
            this.hScaleButton = uicontrol('Style','pushbutton','Parent', this.hControlPanel,'String','Rescale',  'Units', 'normalized', 'Position', [0 0.1 1 0.1], 'Enable', 'off', 'Callback', @cbYAxisScale);               
            
            this.firstSet = this.currentStatisticalSet.getId;
            this.secondSet = this.currentStatisticalSet.getId;
            this.setIndices = [1 1];
            this.firstDiagnose = this.diagnoses.id(1);
            this.secondDiagnose = this.diagnoses.id(1);
            this.diagnoseIndices = [1 1];
            tabData = this.CovTableData;
            colergen = @(color,text) ['<html><table border=0 width=400 bgcolor=',color,'><TR><TD>',text,'</TD></TR> </table></html>'];
            tabData{1,1} = colergen('#75C0F9', tabData{1,1});
            set(this.hCovTable,'Data', tabData);
            
            this.ComparePlot;
            this.FeaturePlot;
            
            function cbCovMatSelection(varargin)
                ind = varargin{2}.Indices;
                if isempty(ind)
                    return;
                end 
                if this.vDistinguishBetween
                    this.firstSet = this.statisticalSets(ind(1));
                    this.secondSet = this.statisticalSets(ind(2));
                else
                    tmpDiag = unique(this.diagnoses.id);
                    this.firstDiagnose = tmpDiag(ind(1));
                    this.secondDiagnose = tmpDiag(ind(2));
                end
                tabData = this.CovTableData;
                colergen = @(color,text) ['<html><table border=0 width=400 bgcolor=',color,'><TR><TD>',text,'</TD></TR> </table></html>'];
                tabData{ind(1),ind(2)} = colergen('#75C0F9', tabData{ind(1),ind(2)});
                set(this.hCovTable,'Data', tabData);
                this.cmChangeStatisticalSet;
            end
            
            function cbDropDownButton(varargin)
                this.vXAxis = this.hXAxisDropDown.Value;
                this.vYAxis = this.hYAxisDropDown.Value;
                this.FeaturePlot;
            end
            
            function cbFeatureAddButton(varargin)
                indices = find(this.brushSelection);
                featuresFlow = [];
                featuresPressure = [];
                l = length(this.selection)*length(this.statisticalSets);
                if ismember(1,this.plotSelection)
                    featuresFlow = zeros(l,length(indices));
                    sets = zeros(l,1);
                    diagnoseId = zeros(l,1);
                    diagnose = cell(l,1);
                    counter = 0;
                    for i = 1:length(this.statisticalSets)
                        setId = this.statisticalSets(i);
                        if this.vLoad
                            sol = this.loadedSolutions(i,:);
                        else
                            tempSet = StatisticalSet('Id', this.statisticalSets(i), this.db);
                            this.psSet = tempSet.Elements;
                            this.psRef = tempSet.getSeed;
                            this.psRefInit;
                            sol = this.readNodeSolutions(this.currentNode.nodeId);
                        end                 
                        for j = 2:length(sol)
                            counter = counter + 1;
                            flow = this.operator(sol(j).data(1,:));
                            len = length(flow);                       
                            interpXAxis = linspace(1, len, this.refLen);
                            flow = interp1(1:len, flow, interpXAxis);
                            featuresFlow(counter,:) = flow(indices);
                            sets(counter) = this.statisticalSets(i);
                            diagnoseId(counter) = this.diagnoses.id(find(this.statisticalSets == setId));
                            diagnose{counter} = this.diagnoses.name{find(this.statisticalSets == setId)};
                        end
                    end
                end
                if ismember(2, this.plotSelection)
                    featuresPressure = zeros(l,length(indices));
                    sets = zeros(l,1);
                    diagnoseId = zeros(l,1);
                    diagnose = cell(l,1);
                    counter = 0;
                    for i = 1:length(this.statisticalSets)
                        setId = this.statisticalSets(i);
                        if this.vLoad
                            sol = this.loadedSolutions(i,:);
                        else
                            tempSet = StatisticalSet('Id', this.statisticalSets(i), this.db);
                            this.psSet = tempSet.Elements;
                            this.psRef = tempSet.getSeed;
                            this.psRefInit;
                            sol = this.readNodeSolutions(this.currentNode.nodeId);
                        end
                        for j = 2:length(sol)
                            counter = counter + 1;
                            pressure = this.operator(sol(j).data(2,:));
                            len = length(pressure);                       
                            interpXAxis = linspace(1, len, this.refLen);
                            pressure = interp1(1:len, pressure, interpXAxis);
                            featuresPressure(counter,:) = pressure(indices);
                            sets(counter) = this.statisticalSets(i);
                            diagnoseId(counter) = this.diagnoses.id(find(this.statisticalSets == setId));
                            diagnose{counter} = this.diagnoses.name{find(this.statisticalSets == setId)};
                        end
                    end
                end
                this.psSet = this.currentStatisticalSet.Elements; 
                this.psRef = this.currentStatisticalSet.getSeed;
                this.psRefInit;
                txt = '';
                if this.vGradientCompare
                    txt = 'gradient ';
                end
                if this.vAmplitudeSpec
                    txt = [txt,'amplitude spectrum '];
                elseif this.vPhaseSpec
                    txt = [txt,'phase spectrum '];
                end 
                if ~isempty(featuresFlow) && ~isempty(featuresPressure)
                    featureMatrix = [featuresFlow,featuresPressure];
                    type = cell(1,size(featureMatrix,2));          
                    [type{1,1:size(featuresFlow,2)}] = deal([txt,'flow']);
                    [type{1,size(featuresFlow,2)+1:size(featuresFlow,2)+size(featuresPressure,2)}] = deal([txt,'pressure']);
                    timeFreq = [this.timeFreq(indices),this.timeFreq(indices)];
                elseif ~isempty(featuresFlow)
                    featureMatrix = featuresFlow;
                    [type{1,1:size(featuresFlow,2)}] = deal([txt,'flow']);
                    timeFreq = this.timeFreq(indices);
                else
                    featureMatrix = featuresPressure;
                    [type{1,1:size(featuresPressure,2)}] = deal([txt,'pressure']);
                    timeFreq = this.timeFreq(indices);
                end
                features.featureMatrix = featureMatrix;
                features.set = sets;
                features.diagnosesId = diagnoseId;
                features.diagnoses = diagnose;
                features.type = type;
                features.timeFreq = timeFreq;
                if ~isempty(this.hClassifierObj)
                    if this.vDistinguishBetween
                        this.hClassifierObj.setClassifyBetween('Sets');
                    else
                        this.hClassifierObj.setClassifyBetween('Diagnose');
                    end
                    this.hClassifierObj.AddFeatures(features);
                end
            end
            
            function cbNewClassifierObj(varargin)
                this.hClassifierObj = Classifier('Show','on');
            end
      
            function cbLoadClassifierObj(varargin)
                [filename,pathname] = uigetfile('*.mat','Select the MATLAB data file');
                if isnumeric(filename)
                    if filename == 0
                        return;
                    end
                end
                filename = fullfile(pathname,filename);
                featureData = load(filename);
                if isstruct(featureData)
                    member = fieldnames(featureData);
                    if length(member) == 1
                        field = datestr(now,member{:});
                        featureData = featureData.(field);
                    end
                end
                this.hClassifierObj = Classifier('Show','on','Data', featureData);
            end
            
            function cmActualDistribution(varargin)
                m = varargin{1};
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off');
                    this.vActualDistribution = false;
                else
                    set(m, 'Checked', 'on');
                    set(this.hcmGaussianDistribution, 'Checked', 'off');
                    this.vActualDistribution = true;
                    this.vGaussianDistribution = false;
                end
                this.FeaturePlot;
            end
            
            function cmGaussianDistribution(varargin)
                m = varargin{1};
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off');
                    this.vGaussianDistribution = false;
                else
                    set(m, 'Checked', 'on');
                    set(this.hcmActualDistribution, 'Checked', 'off');
                    this.vGaussianDistribution = true;
                    this.vActualDistribution = false;
                end
                this.FeaturePlot;
            end    
            
            function cmLinkBrushSelectionUD(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    this.vLinkBrushUD = false; 
                else
                    set(m, 'Checked', 'on')
                    this.vLinkBrushUD = true; 
                end
            end
            
            function cmLinkBrushSelectionLR(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    this.vLinkBrushLR = false; 
                else
                    set(m, 'Checked', 'on')
                    this.vLinkBrushLR = true; 
                end
            end
            
            function cmGradient(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    this.vGradientCompare = false; 
                else
                    set(m, 'Checked', 'on')
                    this.vGradientCompare = true; 
                end
               this.ComparePlot; 
            end

            function cmDistinguishBetween(varargin)
                m = varargin{1};
                if strcmpi(m.Label(end-8:end),'diagnoses')
                    set(this.hcmDistinguishSets, 'Checked', 'off');
                    set(this.hcmDistinguishSets, 'Enable', 'on');
                    this.vDistinguishBetween = 0;
                    l = length(unique(this.diagnoses.id));
                    x = this.diagnoseIndices(1);
                    y = this.diagnoseIndices(2);
                    names = unique(this.diagnoses.name);
                else
                    set(this.hcmDistinguishDiagnoses, 'Checked', 'off');
                    set(this.hcmDistinguishDiagnoses, 'Enable', 'on');
                    this.vDistinguishBetween = 1;
                    l = length(this.statisticalSets);
                    x = this.setIndices(1);
                    y = this.setIndices(2);
                    names = string(this.statisticalSets);
                end
                data = cell(l);
                data(:,:) = {'COV'};
                for i = 1:l
                    data(i,i) = {strcat(char(963),char(178))};
                end
                this.CovTableData = data;
                data{x,y} = colergen('#75C0F9', data{x,y});
                set(this.hCovTable, 'Data', data);
                set(this.hCovTable, 'RowName', names);
                set(this.hCovTable, 'ColumnName', names);
                set(m, 'Checked', 'on');
                set(m, 'Enable', 'off');
                this.ComparePlot;
                this.FeaturePlot;
            end

            function cmAmplitudeSpec(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    set(this.hScaleButton, 'Enable', 'off');
                    this.vAmplitudeSpec = false; 
                else
                    set(m, 'Checked', 'on')
                    set(this.hScaleButton, 'Enable', 'on');
                    set(this.hcmFFTiCompare, 'Checked', 'off')
                    this.vPowerSpecCompare = false; 
                    this.vPhaseSpec = false; 
                    this.vAmplitudeSpec = true; 
                end
               this.ComparePlot;
            end

            function cmPhaseSpec(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    set(this.hScaleButton, 'Enable', 'off');
                    this.vPhaseSpec = false; 
                else
                    set(m, 'Checked', 'on')
                    set(this.hScaleButton, 'Enable', 'on');
                    set(this.hcmFFTCompare, 'Checked', 'off')
                    this.vPowerSpecCompare = false; 
                    this.vAmplitudeSpec = false; 
                    this.vPhaseSpec = true; 
                end
               this.ComparePlot; 
            end
                 
            function cbYAxisScale(varargin)
                if this.div == 8
                    this.div = 2;
                else
                    this.div = this.div + 1;
                end
                this.ComparePlot;
            end
            
            function cbSaveCurrentPlot2Png(varargin)
                if isempty(this.lastClickedPlot)
                    return;
                end
                [filename,pathname] = uiputfile('*.png','Save image as...');
                if isnumeric(filename)
                    if filename == 0
                        return;
                    end
                end
                switch this.lastClickedPlot
                    case 1
                        currentPanel = this.hCovPanelF;
                    case 2
                        currentPanel = this.hCovPanelP;
                    case 3
                        currentPanel = this.hStdDevPanelF;
                    case 4
                        currentPanel = this.hStdDevPanelP;
                end
                fullname = fullfile(pathname,filename);
                u = get(currentPanel, 'units'); 
                set(currentPanel, 'units', 'Pixels'); 
                pos = get(currentPanel, 'Position'); 
                set(currentPanel, 'units', u); 
                h = figure('toolbar','none','Name','Temporary Image Figure','NumberTitle', 'off', 'Position', pos, 'units', u); 
                pos = get(currentPanel, 'Position'); 
                set(currentPanel,'Position', [0 0 1 1]); 
                copyobj(currentPanel, h); 
                set(currentPanel,'Position', pos); 
                hold on;
                selection = this.brushSelection;
                if sum(selection)
                    keyIndices = find(selection > 0);
                    startIndex = keyIndices(1);
                    endIndex = keyIndices(end);
                    startIndices = [startIndex keyIndices(find(diff(keyIndices)>1)+1)];
                    endIndices = [keyIndices(find(diff(keyIndices)>1)) endIndex];
                    lims = get(gca, 'Ylim');
                    children = get(gca, 'Children');
                    xData = children(1).XData;
                    yUpperLim = lims(end); 
                    yLowerLim = lims(1);
                    for recCounter = 1:length(startIndices)
                        if startIndices(recCounter) == endIndices(recCounter)
                            line([xData(startIndices(recCounter)) xData(startIndices(recCounter))], [yLowerLim yUpperLim], 'Color', [.5 .5 .5 .3]); 
                        else
                            rectangle('Position',[xData(startIndices(recCounter)), yLowerLim, xData(endIndices(recCounter)-startIndices(recCounter)), yUpperLim-yLowerLim],...
                                      'FaceColor',[.5 .5 .5 .3] ,'EdgeColor',[.5 .5 .5 .3]);
                        end
                    end     
                end
                print(h, fullname, '-r400','-dpng');
            end
            
            function cbSaveFeaturePlot2Png(varargin)
                [filename,pathname] = uiputfile('*.png','Save image as...');
                if isnumeric(filename)
                    if filename == 0
                        return;
                    end
                end
                fullname = fullfile(pathname,filename);
                u = get(this.hFeaturePanel, 'units'); 
                set(this.hFeaturePanel, 'units', 'Pixels'); 
                pos = get(this.hFeaturePanel, 'Position'); 
                set(this.hFeaturePanel, 'units', u); 
                h = figure('toolbar','none','Name','Temporary Image Figure','NumberTitle', 'off', 'Position', pos, 'units', u); 
                pos = get(this.hFeaturePanel, 'Position'); 
                set(this.hFeaturePanel,'Position', [0 0 1 1]); 
                copyobj(this.hFeaturePanel, h); 
                set(this.hFeaturePanel,'Position', pos); 
                print(h, fullname, '-r400','-dpng');
            end
            
            function cbViewDeviations(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    this.vDeviation = false; 
                else
                    set(m, 'Checked', 'on')
                    this.vDeviation = true; 
                end
                this.FeaturePlot;
            end
            
            function cbViewMeans(varargin)
                m = varargin{1}; 
                if strcmp(get(m, 'Checked'), 'on')
                    set(m, 'Checked', 'off')
                    this.vMean = false; 
                else
                    set(m, 'Checked', 'on')
                    this.vMean = true; 
                end
                this.FeaturePlot;
            end
        end
            
        function ComparePlot(this, varargin)
            set(0, 'CurrentFigure', this.hCompare)
            pos = [0.1 0.2 0.85 0.7];
            b = brush(gcf);
            set(b, 'Color', 'black');
            colors = {[1 0 0];
                      [0 1 0];
                      [0 0 1];
                      [1 1 0];
                      [0 1 1];
                      [1 0 1]};
            hatching = {[1 0.4 0.4];
                        [0.698 1 0.4];
                        [0.4 0.689 1];
                        [1 1 0.4];
                        [0.6 1 1];
                        [1 0.6 1]};
            hPlotsStdDevF = cell(1);
            hPlotsStdDevP = cell(1);
            hPlotsStdDevFU = cell(1);
            hPlotsStdDevFD = cell(1);
            hPlotsStdDevPU = cell(1);
            hPlotsStdDevPD = cell(1);
            if this.vAmplitudeSpec || this.vPhaseSpec
                t = linspace(0,(1/this.delta)/this.div, this.refLen);
            else
                t = this.refT;
            end
            this.timeFreq = t;
            plotCovVar;
            plotStdDev;
            this.psSet = this.currentStatisticalSet.Elements; 
            this.psRef = this.currentStatisticalSet.getSeed;
            this.psRefInit;
            
            function plotCovVar(varargin)
                axesF = subplot(1,1,1, 'Parent', this.hCovPanelF, 'Position', pos, 'Tag', '1');
                axesP = subplot(1,1,1, 'Parent', this.hCovPanelP, 'Position', pos, 'Tag', '2');
                if this.vDistinguishBetween
                    if this.firstSet == this.secondSet
                        node = this.currentNode;
                        if this.vLoad
                            index = find(this.firstSet == this.statisticalSets);
                            sol1 = this.loadedSolutions(index,:);
                            sol2 = this.loadedSolutions(index,:);
                        else
                            sol1 = this.readNodeSolutions(node.nodeId);
                            sol2 = this.readNodeSolutions(node.nodeId);
                        end
                        titl = 'variance ';
                    else
                        node = this.currentNode;
                        if this.vLoad
                            index = find(this.firstSet == this.statisticalSets);
                            sol1 = this.loadedSolutions(index,:);
                            index = find(this.secondSet == this.statisticalSets);
                            sol2 = this.loadedSolutions(index,:);
                        else
                            sol1 = this.readNodeSolutions(node.nodeId);
                            tempSet = StatisticalSet('Id', this.secondSet, this.db);
                            this.psSet = tempSet.Elements;
                            this.psRef = tempSet.getSeed;
                            this.psRefInit;
                            sol2 = this.readNodeSolutions(node.nodeId);
                            this.psSet = this.currentStatisticalSet.Elements; 
                            this.psRef = this.currentStatisticalSet.getSeed;
                            this.psRefInit;
                        end
                        titl = 'covariance ';
                    end
                    if isempty(this.selection) % No Solver Run selected shows mean
                        axes(axesF);
                        cla;
                        axes(axesP);
                        cla;
                    else
                        % Mean
                        flow1 = zeros(1,this.refLen);
                        flow2 = zeros(1,this.refLen);
                        pressure1 = zeros(1,this.refLen);
                        pressure2 = zeros(1,this.refLen);
                        for solCounter = 2:length(sol1)
                            flow1 = flow1 + this.interpolateData(this.operator(sol1(solCounter).data(1,:)));
                            flow2 = flow2 + this.interpolateData(this.operator(sol2(solCounter).data(1,:)));
                            pressure1 = pressure1 + this.interpolateData(this.operator(sol1(solCounter).data(2,:)));
                            pressure2 = pressure2 + this.interpolateData(this.operator(sol2(solCounter).data(2,:)));
                        end
                        meanFlow1 = flow1/solCounter;
                        meanFlow2 = flow2/solCounter;
                        meanPressure1 = pressure1/solCounter;
                        meanPressure2 = pressure2/solCounter;
                        % Variance/Covariance
                        covVarFlow = zeros(1,this.refLen);
                        covVarPressure = zeros(1,this.refLen);
                        for solCounter = 2:length(sol1)
                            flow1 = this.interpolateData(this.operator(sol1(solCounter).data(1,:)));
                            flow2 = this.interpolateData(this.operator(sol2(solCounter).data(1,:)));
                            pressure1 = this.interpolateData(this.operator(sol1(solCounter).data(2,:)));
                            pressure2 = this.interpolateData(this.operator(sol2(solCounter).data(2,:)));
                            covVarFlow = covVarFlow + ((flow1-meanFlow1).*(flow2-meanFlow2));
                            covVarPressure = covVarPressure + ((pressure1-meanPressure1).*(pressure2-meanPressure2));
                        end
                        covVarFlow = covVarFlow/(solCounter-1);
                        covVarPressure = covVarPressure/(solCounter-1);
                        axes(axesF);
                        this.hPlotF = plot(t, covVarFlow, 'b-' ,'Linewidth', this.fatness+1, 'Color', [1 0 0]);
                        axes(axesP);
                        this.hPlotP = plot(t, covVarPressure, 'b-' ,'Linewidth', this.fatness+1, 'Color', [1 0 0]);
                    end
                else
                    if this.firstDiagnose == this.secondDiagnose
                        titl = 'variance ';
                    else
                        titl = 'covariance ';
                    end
                    if ~isempty(this.selection)
                        tmpDiag = unique(this.diagnoses.id);      
                        setIndices = find(this.diagnoses.id == this.firstDiagnose);
                        sets1 = this.statisticalSets(setIndices);
                        setIndices = find(this.diagnoses.id == this.secondDiagnose);
                        sets2 = this.statisticalSets(setIndices);
                        meanFlow1 =  zeros(1, this.refLen);
                        meanFlow2 =  zeros(1, this.refLen);
                        meanPressure1 =  zeros(1, this.refLen);
                        meanPressure2 =  zeros(1, this.refLen);
                        div = 0;
                        for setCounter = 1:length(sets1)
                            if this.vLoad
                                index = find(sets1(setCounter) == this.statisticalSets);
                                sol = this.loadedSolutions(index,:);
                            else
                                tmpStatisticalSet = StatisticalSet('Id', sets1(setCounter), this.db); 
                                this.psSet = tmpStatisticalSet.Elements; 
                                this.psRef = tmpStatisticalSet.getSeed;
                                sol = this.readNodeSolutions(this.currentNode.nodeId);
                            end
                            for solCounter = 2:length(sol)
                                meanFlow1 = meanFlow1 + this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                meanPressure1 = meanPressure1 + this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                                div = div + 1;
                            end                                
                        end
                        meanFlow1 = meanFlow1/div;
                        meanPressure1 = meanPressure1/div;
                        div = 0;
                        for setCounter = 1:length(sets2)
                            if this.vLoad
                                index = find(sets2(setCounter) == this.statisticalSets);
                                sol = this.loadedSolutions(index,:);
                            else
                                tmpStatisticalSet = StatisticalSet('Id', sets2(setCounter), this.db); 
                                this.psSet = tmpStatisticalSet.Elements; 
                                this.psRef = tmpStatisticalSet.getSeed;
                                sol = this.readNodeSolutions(this.currentNode.nodeId);
                            end                                       
                            for solCounter = 2:length(sol)
                                meanFlow2 = meanFlow2 + this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                meanPressure2 = meanPressure2 + this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                                div = div + 1;
                            end                                
                        end
                        meanFlow2 = meanFlow2/div;
                        meanPressure2 = meanPressure2/div;

                        lastSol1 = false;
                        lastSol2 = false;
                        setCounter1 = 1;
                        setCounter2 = 1;
                        flow = zeros(1, this.refLen);
                        pressure = zeros(1, this.refLen);
                        solCounter1 = 1;
                        solCounter2 = 1;
                        div = 0;
                        while (setCounter1 <= length(sets1) || setCounter2 <= length(sets2))
                            if solCounter1 == 1 || lastSol1
                                if this.vLoad
                                    index = find(sets1(setCounter) == this.statisticalSets);
                                    sol1 = this.loadedSolutions(index,:);
                                else
                                    tmpStatisticalSet = StatisticalSet('Id', sets1(setCounter1), this.db); 
                                    this.psSet = tmpStatisticalSet.Elements; 
                                    this.psRef = tmpStatisticalSet.getSeed;
                                    sol1 = this.readNodeSolutions(this.currentNode.nodeId);
                                end
                                sol1 = sol1(2:end);
                                l1 = length(sol1);
                                lastSol1 = false;
                                solCounter1 = 1;
                            end
                            if solCounter2 == 1 || lastSol2
                                if this.vLoad
                                    index = find(sets2(setCounter) == this.statisticalSets);
                                    sol2 = this.loadedSolutions(index,:);
                                else
                                   tmpStatisticalSet = StatisticalSet('Id', sets2(setCounter2), this.db); 
                                    this.psSet = tmpStatisticalSet.Elements; 
                                    this.psRef = tmpStatisticalSet.getSeed;
                                    sol2 = this.readNodeSolutions(this.currentNode.nodeId);
                                end
                                sol2 = sol2(2:end);
                                l2 = length(sol2);
                                lastSol2 = false;
                                solCounter2 = 1;
                            end
                            interpFlow1 = this.interpolateData(this.operator(sol1(solCounter1).data(1,:)));
                            interpFlow2 = this.interpolateData(this.operator(sol2(solCounter2).data(1,:)));
                            interpPressure1 = this.interpolateData(this.operator(sol1(solCounter1).data(2,:)));
                            interpPressure2 = this.interpolateData(this.operator(sol2(solCounter2).data(2,:)));  
                            flow = flow + ((interpFlow1-meanFlow1).*(interpFlow2-meanFlow2));
                            pressure = pressure + ((interpPressure1-meanPressure1).*(interpPressure2-meanPressure2));
                            if l1 == solCounter1
                                lastSol1 = true;
                                setCounter1 = setCounter1 + 1;
                            end
                            if l2 == solCounter2
                                lastSol2 = true;
                                setCounter2 = setCounter2 + 1;
                            end
                            solCounter1 = solCounter1 + 1;
                            solCounter2 = solCounter2 + 1;
                            div = div + 1;
                        end
                        covVarFlow = flow/(div-1);
                        covVarPressure = pressure/(div-1);
                        axes(axesF);
                        this.hPlotF = plot(t, covVarFlow, 'b-' ,'Linewidth', this.fatness+1, 'Color', [1 0 0]);
                        axes(axesP);
                        this.hPlotP = plot(t, covVarPressure, 'b-' ,'Linewidth', this.fatness+1, 'Color', [1 0 0]);
                    end
                end
                txt = '';
                lablX = 'time [s]';
                lablYF = 'flow [ml/s]';
                lablYP = 'pressure [Pa]';
                if this.vAmplitudeSpec
                    txt = 'of FFT real part ';
                    lablX = 'frequency [Hz]';
                    lablYF = 'Amplitude';
                    lablYP = 'Amplitude';
                elseif this.vPhaseSpec
                    txt = 'of FFT imaginary part ';
                    lablX = 'frequency [Hz]';
                    lablYF = 'Phase';
                    lablYP = 'Phase';
                elseif this.vPowerSpecCompare
                    txt = 'of power spectrum ';
                    lablX = 'frequency [Hz]';
                    lablYF = '';
                    lablYP = '';
                end
                if this.vGradientCompare
                    txt = [txt,'of gradient '];
                end    
                axes(axesF);
                title([titl,txt,'of flow [node ', num2str(this.currentNode.nodeId),']'], 'Fontsize', 12);
                xlabel(gca, lablX);
                ylabel(gca, lablYF);
                set(gca, 'Tag', '1');
                axes(axesP);
                title([titl,txt,'of pressure [node ', num2str(this.currentNode.nodeId),']'], 'Fontsize', 12);
                xlabel(gca, lablX);
                ylabel(gca, lablYP);
                set(gca, 'Tag', '2');
            end
            
            function plotStdDev(varargin)
                axesF = subplot(1,1,1, 'Parent', this.hStdDevPanelF, 'Position', pos);
                axesP = subplot(1,1,1, 'Parent', this.hStdDevPanelP, 'Position', pos);
                if this.vDistinguishBetween
                    legnd = string(this.statisticalSets);
                    for setCounter = 1:length(this.statisticalSets)
                        if this.vLoad
                            index = find(this.statisticalSets(setCounter) == this.statisticalSets);
                            sol = this.loadedSolutions(index,:);
                        else
                            currentStatisticalSet = StatisticalSet('Id', this.statisticalSets(setCounter), this.db);   
                            this.psSet = currentStatisticalSet.Elements; 
                            this.psRef = currentStatisticalSet.getSeed;
                            this.psRefInit;
                            sol = this.readNodeSolutions(this.currentNode.nodeId);
                        end
                        if length(sol) == 1
                            set(b,'enable','off', 'ActionPostCallback', @cbBrushSelection);  
                            axes(axesF);
                            hPlotsStdDevF{setCounter} = plot(t, sol(1).data(1,:), 'Linewidth', 1+this.fatness, 'Color', colors{setCounter}, 'UserData', 0); 
                            hold on;
                            axes(axesP);
                            hPlotsStdDevP{setCounter} = plot(t, sol(1).data(2,:), 'Linewidth', 1+this.fatness, 'Color', colors{setCounter}, 'UserData', 0); 
                            hold on;
                        else
                            set(b,'enable','on','Color', [0 0 0],'ActionPostCallback', @cbBrushSelection);  
                            %Mean
                            flow = zeros(1,this.refLen);
                            pressure = zeros(1,this.refLen);
                            for solCounter = 2:length(sol)
                                flow = flow + this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                pressure = pressure + this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                            end
                            meanFlow = flow/solCounter;
                            meanPressure = pressure/solCounter;
                            % Standard Deviation
                            stdDevFlow = zeros(1,this.refLen);
                            stdDevPressure = zeros(1,this.refLen);
                            for solCounter = 2:length(sol)
                                flow = this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                pressure = this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                                stdDevFlow = stdDevFlow + ((flow-meanFlow).*(flow-meanFlow));
                                stdDevPressure = stdDevPressure + ((pressure-meanPressure).*(pressure-meanPressure));
                            end
                            stdDevFlow = sqrt(stdDevFlow/(solCounter-1));
                            stdDevPressure = sqrt(stdDevPressure/(solCounter-1));
                            axes(axesF);
                            fill([t;t],[(meanFlow - stdDevFlow);(meanFlow + stdDevFlow)], hatching{setCounter},'Edgecolor', hatching{setCounter})
                            hold on; 
                            hPlotsStdDevFD{setCounter} = plot(t,(meanFlow - stdDevFlow), 'b:' ,'Linewidth', this.fatness+1, 'Color', hatching{setCounter});
                            hPlotsStdDevFU{setCounter} = plot(t, (meanFlow + stdDevFlow), 'b:', 'Linewidth', this.fatness+1, 'Color', hatching{setCounter});
                            hPlotsStdDevF{setCounter} = plot(t, meanFlow, 'Linewidth', 1+this.fatness, 'Color', colors{setCounter}, 'UserData', 0);
                            axes(axesP);
                            fill([t;t],[(meanPressure - stdDevPressure);(meanPressure + stdDevPressure)], hatching{setCounter},'Edgecolor', hatching{setCounter})
                            hold on; 
                            hPlotsStdDevPD{setCounter} = plot(t,(meanPressure - stdDevPressure), 'b:' ,'Linewidth', this.fatness+1, 'Color', hatching{setCounter});
                            hPlotsStdDevPU{setCounter} = plot(t, (meanPressure + stdDevPressure), 'b:', 'Linewidth', this.fatness+1, 'Color', hatching{setCounter});
                            hPlotsStdDevP{setCounter} = plot(t, meanPressure, 'Linewidth', 1+this.fatness, 'Color', colors{setCounter}, 'UserData', 0);
                        end
                    end
                else
                    legnd = unique(this.diagnoses.name); 
                    for diagnoseCounter = 1:length(unique(this.diagnoses.id))
                        tmpDiag = unique(this.diagnoses.id);
                        setIndices = find(this.diagnoses.id == tmpDiag(diagnoseCounter));
                        sets = this.statisticalSets(setIndices);
                        flow = zeros(1,this.refLen);
                        pressure = zeros(1,this.refLen);
                        stdDevFlow = zeros(1,this.refLen);
                        stdDevPressure = zeros(1,this.refLen);
                        div = 0;
                        for setCounter = 1:length(sets)
                            if this.vLoad
                                index = find(sets(setCounter) == this.statisticalSets);
                                sol = this.loadedSolutions(index,:);
                            else
                                currentStatisticalSet = StatisticalSet('Id', sets(setCounter), this.db);   
                                this.psSet = currentStatisticalSet.Elements; 
                                this.psRef = currentStatisticalSet.getSeed;
                                this.psRefInit;
                                sol = this.readNodeSolutions(this.currentNode.nodeId);
                            end
                            if length(sol) == 1
                                set(b,'enable','off', 'ActionPostCallback', @cbBrushSelection);  
                                axes(axesF);
                                hPlotsStdDevF{diagnoseCounter} = plot(t, sol(1).data(1,:), 'Linewidth', 1+this.fatness, 'Color', colors{diagnoseCounter}, 'UserData', 0); 
                                hold on;
                                axes(axesP);
                                hPlotsStdDevP{diagnoseCounter} = plot(t, sol(1).data(2,:), 'Linewidth', 1+this.fatness, 'Color', colors{diagnoseCounter}, 'UserData', 0); 
                                hold on;
                            else
                                set(b,'enable','on','Color', [0 0 0],'ActionPostCallback', @cbBrushSelection);  
                                for solCounter = 2:length(sol)
                                    div = div + 1;
                                    flow = flow + this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                    pressure = pressure + this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                                end
                            end    
                        end
                        meanFlow = flow/div;
                        meanPressure = pressure/div;
                        for setCounter = 1:length(sets)
                            if length(sol) ~= 1
                                for solCounter = 2:length(sol)
                                    flow = this.interpolateData(this.operator(sol(solCounter).data(1,:)));
                                    pressure = this.interpolateData(this.operator(sol(solCounter).data(2,:)));
                                    stdDevFlow = stdDevFlow + ((flow-meanFlow).*(flow-meanFlow));
                                    stdDevPressure = stdDevPressure + ((pressure-meanPressure).*(pressure-meanPressure));
                                end
                            end
                        end
                        stdDevFlow = sqrt(stdDevFlow/(div-1));
                        stdDevPressure = sqrt(stdDevPressure/(div-1));
                        axes(axesF);
                        fill([t;t],[(meanFlow - stdDevFlow);(meanFlow + stdDevFlow)], hatching{diagnoseCounter},'Edgecolor', hatching{diagnoseCounter})
                        hold on; 
                        hPlotsStdDevFD{diagnoseCounter} = plot(t,(meanFlow - stdDevFlow), 'b:' ,'Linewidth', this.fatness+1, 'Color', hatching{diagnoseCounter});
                        hPlotsStdDevFU{diagnoseCounter} = plot(t, (meanFlow + stdDevFlow), 'b:', 'Linewidth', this.fatness+1, 'Color', hatching{diagnoseCounter});
                        hPlotsStdDevF{diagnoseCounter} = plot(t, meanFlow, 'Linewidth', 1+this.fatness, 'Color', colors{diagnoseCounter}, 'UserData', 0);
                        axes(axesP);
                        fill([t;t],[(meanPressure - stdDevPressure);(meanPressure + stdDevPressure)], hatching{diagnoseCounter},'Edgecolor', hatching{diagnoseCounter})
                        hold on; 
                        hPlotsStdDevPD{diagnoseCounter} = plot(t,(meanPressure - stdDevPressure), 'b:' ,'Linewidth', this.fatness+1, 'Color', hatching{diagnoseCounter});
                        hPlotsStdDevPU{diagnoseCounter} = plot(t, (meanPressure + stdDevPressure), 'b:', 'Linewidth', this.fatness+1, 'Color', hatching{diagnoseCounter});
                        hPlotsStdDevP{diagnoseCounter} = plot(t, meanPressure, 'Linewidth', 1+this.fatness, 'Color', colors{diagnoseCounter}, 'UserData', 0);                              
                    end
                end
                txt = '';
                lablX = 'time [s]';
                lablYF = 'flow [ml/s]';
                lablYP = 'pressure [Pa]';
                if this.vAmplitudeSpec
                    txt = 'of FFT real part ';
                    lablX = 'frequency [Hz]';
                    lablYF = 'Amplitude';
                    lablYP = 'Amplitude';
                elseif this.vPhaseSpec
                    txt = 'of FFT imaginary part ';
                    lablX = 'frequency [Hz]';
                    lablYF = 'Phase';
                    lablYP = 'Phase';
                elseif this.vPowerSpecCompare
                    txt = 'of power spectrum ';
                    lablX = 'frequency [Hz]';
                    lablYF = '';
                    lablYP = '';
                end
                if this.vGradientCompare
                    txt = [txt,'of gradient '];
                end
                axes(axesF);
                legend([hPlotsStdDevF{:}],legnd);
                title(['standard deviation ',txt,'of flow [node ', num2str(this.currentNode.nodeId),']'], 'Fontsize', 12);
                xlabel(gca, lablX);
                ylabel(gca, lablYF);
                set(gca, 'Tag', '3');
                axes(axesP);
                legend([hPlotsStdDevP{:}],legnd);
                title(['standard deviation ',txt,'of pressure [node ', num2str(this.currentNode.nodeId),']'], 'Fontsize', 12);
                xlabel(gca, lablX);
                ylabel(gca, lablYP);
                set(gca, 'Tag', '4');
                this.psSet = this.currentStatisticalSet.Elements; 
                this.psRef = this.currentStatisticalSet.getSeed;
                this.psRefInit;
            end
                   
            function cbBrushSelection(varargin)
                tag = str2double(varargin{1,2}.Axes.Tag);
                if isnan(tag)
                    return;
                end
                this.lastClickedPlot = tag;
                switch  tag
                    case 1
                        brushSelection = get(this.hPlotF,'brushdata');
                    case 2
                        brushSelection = get(this.hPlotP,'brushdata');
                    case 3
                        brushSelection = zeros(this.refLen,1);
                        for i = 1:length(hPlotsStdDevF)
                            plt = hPlotsStdDevF{i};
                            if nnz(get(plt,'brushdata')) > nnz(brushSelection)
                                brushSelection = get(plt,'brushdata');
                            end
                        end
                    case 4
                        brushSelection = zeros(this.refLen,1);
                        for i = 1:length(hPlotsStdDevP)
                            plt = hPlotsStdDevP{i};
                            if nnz(get(plt,'brushdata')) > nnz(brushSelection)
                                brushSelection = get(plt,'brushdata');
                            end
                        end
                end
                this.brushSelection = double(brushSelection > 0);
                check = tag;
                if this.vLinkBrushLR
                    switch check
                        case 1
                            check(end+1) = 3;
                        case 2
                            check(end+1) = 4;
                        case 3
                            check(end+1) = 1;
                        case 4
                            check(end+1) = 2;
                    end
                end
                if this.vLinkBrushUD
                    for checkCounter = 1:2
                        switch check(checkCounter)
                            case 1
                                check(end+1) = 2;
                            case 2
                                check(end+1) = 1;
                            case 3
                                check(end+1) = 4;
                            case 4
                                check(end+1) = 3;
                        end
                    end
                end
                zeroedBrushSelection = zeros(length(brushSelection),1);
                set(this.hPlotF, 'brushdata', zeroedBrushSelection);
                set(this.hPlotP, 'brushdata', zeroedBrushSelection);
                this.plotSelection = [];
                for i = 1:length(hPlotsStdDevP)
                    set(hPlotsStdDevF{i}, 'brushdata', zeroedBrushSelection);
                    set(hPlotsStdDevFU{i}, 'brushdata', zeroedBrushSelection);
                    set(hPlotsStdDevFD{i}, 'brushdata', zeroedBrushSelection);
                    set(hPlotsStdDevP{i}, 'brushdata', zeroedBrushSelection);
                    set(hPlotsStdDevPU{i}, 'brushdata', zeroedBrushSelection);
                    set(hPlotsStdDevPD{i}, 'brushdata', zeroedBrushSelection);
                end
                for axesCounter = 1:length(check)
                    currentAxes = check(axesCounter);
                    switch currentAxes
                        case 1
                            set(this.hPlotF, 'brushdata', brushSelection);
                        case 2
                            set(this.hPlotP, 'brushdata', brushSelection);
                        case 3
                            for i = 1:length(hPlotsStdDevF)
                                set(hPlotsStdDevF{i}, 'brushdata', brushSelection);
                                set(hPlotsStdDevFU{i}, 'brushdata', brushSelection);
                                set(hPlotsStdDevFD{i}, 'brushdata', brushSelection);
                            end
                        case 4
                            for i = 1:length(hPlotsStdDevP)
                                set(hPlotsStdDevP{i}, 'brushdata', brushSelection);
                                set(hPlotsStdDevPU{i}, 'brushdata', brushSelection);
                                set(hPlotsStdDevPD{i}, 'brushdata', brushSelection);
                            end
                    end
                end
                if sum(brushSelection)
                    set(this.hFeatureAddButton, 'Enable', 'on');
                    l = sum(brushSelection > 0);
                    i = 0;
                    pLabel = ', p';
                    qLabel = ', q';
                    if this.vGradientCompare
                        pLabel = [pLabel, ' gradient'];
                        qLabel = [qLabel, ' gradient'];
                    end
                    if this.vAmplitudeSpec
                        pLabel = [pLabel, ' amplitude'];
                        qLabel = [qLabel, ' amplitude'];
                    elseif this.vPhaseSpec
                        pLabel = [pLabel, ' phase'];
                        qLabel = [qLabel, ' phase'];
                    end
                    if any(ismember([1 3],check))
                        i = i + 1;
                        this.plotSelection(end+1) = 1;
                    end
                    if any(ismember([2 4],check))
                        i = i + 1;
                        this.plotSelection(end+1) = 2;
                    end
                    l2 = l*i;
                    list = string(1:l2);  
                    if l == l2
                        if this.plotSelection == 1
                            labl = qLabel;
                        else
                            labl = pLabel;
                        end
                        for j = 1:l2
                            list{j} = [list{j},labl];
                        end
                    else
                        j = 0;
                        while j < l2
                            j = j+1;
                            if j <= l
                                list{j} = [list{j}, qLabel];
                            else
                                list{j} = [list{j}, pLabel];
                            end
                        end
                    end
                    set(this.hXAxisDropDown, 'Value', 1);  
                    set(this.hYAxisDropDown, 'Value', 1);
                    this.vXAxis = 1;
                    this.vYAxis = 1;
                    set(this.hXAxisDropDown, 'String', list);
                    set(this.hYAxisDropDown, 'String', list);
                else
                    set(this.hXAxisDropDown, 'Value', 1);  
                    set(this.hYAxisDropDown, 'Value', 1);
                    this.vXAxis = 1;
                    this.vYAxis = 1;
                    set(this.hFeatureAddButton, 'Enable', 'off');
                    set(this.hXAxisDropDown, 'String', {''});
                    set(this.hYAxisDropDown, 'String', {''});
                end
                this.FeaturePlot;
            end
          
        end

        function FeaturePlot(this, varargin)
            set(0, 'CurrentFigure', this.hCompare);
            this.hScatter = cell(1);
            colors = {[1 0 0];
                      [0 1 0];
                      [0 0 1];
                      [1 1 0];
                      [0 1 1];
                      [1 0 1]};
            meanX = [];
            meanY = [];
            devX = [];
            devY = [];
            this.hFeatureAxes = subplot(1,1,1, 'Parent', this.hFeaturePanel, 'Position', [0.1 0.2 0.825 0.7]);
            title('feature plot', 'Fontsize', 12);
            xlabel('x');
            ylabel('y');
            if ~any(this.brushSelection)
                return;
            end
            if this.vActualDistribution || this.vGaussianDistribution
                plotDistribution;  
            else
                plotFeature;  
            end
            if this.vMean
                plotMean; 
            end
            if this.vDeviation
                plotDeviation;  
            end
            this.psSet = this.currentStatisticalSet.Elements; 
            this.psRef = this.currentStatisticalSet.getSeed;
            this.psRefInit;
            
            function plotFeature(varargin)
                if this.vDistinguishBetween
                    legnd = string(this.statisticalSets);
                    for j = 1:length(this.statisticalSets)
                        if this.vLoad
                            index = find(this.statisticalSets(j) == this.statisticalSets);
                            sol = this.loadedSolutions(index,:);
                        else
                            currentStatisticalSet = StatisticalSet('Id', this.statisticalSets(j), this.db);   
                            this.psSet = currentStatisticalSet.Elements; 
                            this.psRef = currentStatisticalSet.getSeed;
                            this.psRefInit;
                            sol = this.readNodeSolutions(this.currentNode.nodeId);
                        end
                        x = [];
                        y = [];
                        for i = 2:length(sol)
                            data = [];
                            if ismember(1, this.plotSelection)
                                tmpData = this.interpolateData(this.operator(sol(i).data(1,:)));
                                data = [data,tmpData(find(this.brushSelection))];
                            end
                            if ismember(2, this.plotSelection)
                                tmpData = this.interpolateData(this.operator(sol(i).data(2,:)));
                                data = [data,tmpData(find(this.brushSelection))];
                            end
                            x = [x, data(this.vXAxis)];
                            y = [y, data(this.vYAxis)];
                        end
                        meanX(j) = mean(x);
                        meanY(j) = mean(y);
                        devX(j) = var(x);
                        devY(j) = var(y);
                        this.hScatter{j} = scatter(x, y, 'Marker', 'x', 'MarkerEdgeColor', colors{j}, 'Tag', num2str(this.statisticalSets(j)));
                        hold on;
                    end
                else
                    legnd = unique(this.diagnoses.name);
                    for diagnoseCounter = 1:length(unique(this.diagnoses.id))
                        tmpDiag = unique(this.diagnoses.id);
                        setIndices = find(this.diagnoses.id == tmpDiag(diagnoseCounter));
                        sets = this.statisticalSets(setIndices);
                        xAll = [];
                        yAll = [];
                        for j = 1:length(sets)
                            if this.vLoad
                                index = find(sets(j) == this.statisticalSets);
                                sol = this.loadedSolutions(index,:);
                            else
                                currentStatisticalSet = StatisticalSet('Id', sets(j), this.db);   
                                this.psSet = currentStatisticalSet.Elements; 
                                this.psRef = currentStatisticalSet.getSeed;
                                this.psRefInit;
                                sol = this.readNodeSolutions(this.currentNode.nodeId);
                            end
                            x = [];
                            y = [];
                            for i = 2:length(sol)
                                data = [];
                                if ismember(1, this.plotSelection)
                                    tmpData = this.interpolateData(this.operator(sol(i).data(1,:)));
                                    data = [data,tmpData(find(this.brushSelection))];
                                end
                                if ismember(2, this.plotSelection)
                                    tmpData = this.interpolateData(this.operator(sol(i).data(2,:)));
                                    data = [data,tmpData(find(this.brushSelection))];
                                end
                                x = [x, data(this.vXAxis)];
                                y = [y, data(this.vYAxis)];
                            end
                            xAll = [xAll, x];
                            yAll = [yAll, y];
                            this.hScatter{diagnoseCounter} = scatter(x, y, 'Marker', 'x', 'MarkerEdgeColor', colors{diagnoseCounter}, 'Tag', num2str(this.statisticalSets(j)));
                            hold on;
                        end
                        meanX(diagnoseCounter) = mean(xAll);
                        meanY(diagnoseCounter) = mean(yAll);
                        devX(diagnoseCounter) = var(xAll);
                        devY(diagnoseCounter) = var(yAll);
                    end
                end
                legend([this.hScatter{:}],legnd);
                title('feature plot', 'Fontsize', 12);
                if all(ismember([1 2], this.plotSelection))
                    if sum(this.brushSelection) < this.vXAxis
                        xLabl = 'pressure ';
                        checkX = 0;
                    else
                        xLabl = 'flow ';
                        checkX = 1;
                    end
                    if sum(this.brushSelection) < this.vYAxis
                        yLabl = 'pressure ';
                        checkY = 0;
                    else
                        yLabl = 'flow ';
                        checkY = 1;
                    end
                else
                    if ismember(1, this.plotSelection)
                        checkX = 1;
                        checkY = 1;
                        xLabl = 'flow ';
                        yLabl = 'flow ';
                    else
                        checkX = 0;
                        checkY = 0;
                        xLabl = 'pressure ';
                        yLabl = 'pressure ';
                    end
                end
                if this.vAmplitudeSpec
                    xLabl = ['amplitude ',xLabl]; 
                    yLabl = ['amplitude ',yLabl]; 
                elseif this.vPhaseSpec
                    xLabl = ['phase ',xLabl]; 
                    yLabl = ['phase ',yLabl];
                else
                    if checkX
                        xLabl = [xLabl,'[ml/s]'];
                    else
                        xLabl = [xLabl,'[Pa]'];
                    end
                    if checkY
                        yLabl = [yLabl,'[ml/s]'];
                    else
                        yLabl = [yLabl,'[Pa]'];
                    end
                end
                if this.vGradientCompare
                    xLabl = ['gradient ',xLabl]; 
                    yLabl = ['gradient ',yLabl]; 
                end
                xlabel(gca, xLabl);
                ylabel(gca, yLabl);
                hold off;
            end
            
            function plotDistribution(varargin)
                type = cell(1);
                trueColors = [];
                x = [];
                y = [];
                if this.vDistinguishBetween
                    for j = 1:length(this.statisticalSets)
                        if this.vLoad
                            index = find(this.statisticalSets(j) == this.statisticalSets);
                            sol = this.loadedSolutions(index,:);
                        else
                            currentStatisticalSet = StatisticalSet('Id', this.statisticalSets(j), this.db);   
                            this.psSet = currentStatisticalSet.Elements; 
                            this.psRef = currentStatisticalSet.getSeed;
                            this.psRefInit;
                            sol = this.readNodeSolutions(this.currentNode.nodeId);
                        end
                        tmpX = [];
                        tmpY = [];
                        trueColors(end+1) = j;
                        for i = 2:length(sol)
                            data = [];
                            if ismember(1, this.plotSelection)
                                tmpData = this.interpolateData(this.operator(sol(i).data(1,:)));
                                data = [data,tmpData(find(this.brushSelection))];
                            end
                            if ismember(2, this.plotSelection)
                                tmpData = this.interpolateData(this.operator(sol(i).data(2,:)));
                                data = [data,tmpData(find(this.brushSelection))];
                            end
                            tmpX = [tmpX, data(this.vXAxis)];
                            tmpY = [tmpY, data(this.vYAxis)];
                        end
                        x = [x, tmpX];
                        y = [y, tmpY];
                        meanX(j) = mean(tmpX);
                        meanY(j) = mean(tmpY);
                        devX(j) = var(tmpX);
                        devY(j) = var(tmpY);
                        if length(type) == 1
                            [type{1:length(tmpX)}] = deal(num2str(this.statisticalSets(j)));
                        else
                            [type{length(type)+1:length(type)+length(tmpX)}] = deal(num2str(this.statisticalSets(j)));
                        end   
                    end
                    x = reshape(x, [size(x,2),size(x,1)]);
                    y = reshape(y, [size(y,2),size(y,1)]);
                    type = reshape(type, [size(type,2),size(type,1)]);
                    tmpColors = cell2mat(colors);
                    h = scatterhist(x, y,'Group', type,'Marker', 'x', 'Color', tmpColors(trueColors,:));    
                    hists = [copy(h(2).Children), copy(h(3).Children)];
                    axes(h(2));
                    cla;
                    axes(h(3));
                    cla;
                    for j = 1:size(hists,1)
                        if this.vActualDistribution
                            inter = 'pchip';
                        elseif this.vGaussianDistribution
                            inter = 'linear';
                        end
                        ax = h(2);
                        axes(ax);
                        hist = hists(j,1);
                        if isstr(hist.FaceColor)
                            tmpCol = hist.EdgeColor;
                        else
                            tmpCol = hist.FaceColor;
                        end
                        yData = hist.BinCounts;
                        yData = interp1(1:length(yData), yData, 1:0.1:length(yData), inter);
                        xData = linspace(hist.BinLimits(1),hist.BinLimits(2),length(yData));
                        if this.vActualDistribution
                            plot(xData, yData, 'Color', tmpCol, 'LineWidth', 2);
                            hold on;
                        end
                        if this.vGaussianDistribution
                            sig = std(x(string(this.statisticalSets(find(ismember(cell2mat(colors),tmpCol, 'rows')))) == type));
                            mu = mean(x(string(this.statisticalSets(find(ismember(cell2mat(colors),tmpCol, 'rows')))) == type));
                            yStdDevData = normpdf(xData, mu, sig);
                            yStdDevData = yStdDevData * max(yData)/max(yStdDevData);
                            plot(xData, yStdDevData, 'Color', tmpCol, 'LineWidth', 2, 'Linestyle',':');
                            hold on;
                        end
                        ax = h(3);
                        axes(ax);
                        hist = hists(j,2);
                        yData = hist.BinCounts;
                        yData = interp1(1:length(yData), yData, 1:0.1:length(yData),inter);
                        xData = linspace(hist.BinLimits(1),hist.BinLimits(2),length(yData));
                        if this.vActualDistribution 
                            plot(xData, yData, 'Color', tmpCol, 'LineWidth', 2);
                            hold on;
                        end
                        if this.vGaussianDistribution
                            sig = std(y(string(this.statisticalSets(find(ismember(cell2mat(colors),tmpCol, 'rows')))) == type));
                            mu = mean(y(string(this.statisticalSets(find(ismember(cell2mat(colors),tmpCol, 'rows')))) == type));
                            yStdDevData = normpdf(xData, mu, sig);
                            yStdDevData = yStdDevData * max(yData)/max(yStdDevData);
                            plot(xData, yStdDevData, 'Color', tmpCol, 'LineWidth', 2, 'Linestyle',':');
                            hold on;
                        end
                    end
                    set(h(2),'Color','none');
                    set(h(3),'Color','none');
                    set(h(2),'Visible','off');
                    set(h(3),'Visible','off');
                    ax.YDir = 'reverse';
                    ax.View = [270, 90];
                    this.psSet = this.currentStatisticalSet.Elements; 
                    this.psRef = this.currentStatisticalSet.getSeed;
                    this.psRefInit;
                else
                    tmpDiag = unique(this.diagnoses.id); 
                    tmpName = unique(this.diagnoses.name);
                    for diagnoseCounter = 1:length(unique(this.diagnoses.id))
                        setIndices = find(this.diagnoses.id == tmpDiag(diagnoseCounter));
                        sets = this.statisticalSets(setIndices);
                        trueColors(end+1) = diagnoseCounter;
                        xAll = [];
                        yAll = [];
                        for j = 1:length(sets)
                            if this.vLoad
                                index = find(sets(j) == this.statisticalSets);
                                sol = this.loadedSolutions(index,:);
                            else
                                currentStatisticalSet = StatisticalSet('Id', sets(j), this.db);   
                                this.psSet = currentStatisticalSet.Elements; 
                                this.psRef = currentStatisticalSet.getSeed;
                                this.psRefInit;
                                sol = this.readNodeSolutions(this.currentNode.nodeId);
                            end
                            tmpX = [];
                            tmpY = [];
                            for i = 2:length(sol)
                                data = [];
                                if ismember(1, this.plotSelection)
                                    tmpData = this.interpolateData(this.operator(sol(i).data(1,:)));
                                    data = [data,tmpData(find(this.brushSelection))];
                                end
                                if ismember(2, this.plotSelection)
                                    tmpData = this.interpolateData(this.operator(sol(i).data(2,:)));
                                    data = [data,tmpData(find(this.brushSelection))];
                                end
                                tmpX = [tmpX, data(this.vXAxis)];
                                tmpY = [tmpY, data(this.vYAxis)];
                            end
                            x = [x, tmpX];
                            y = [y, tmpY]; 
                            xAll = [xAll, tmpX];
                            yAll = [yAll, tmpY];
                        end
                        meanX(diagnoseCounter) = mean(xAll);
                        meanY(diagnoseCounter) = mean(yAll);
                        devX(diagnoseCounter) = var(xAll);
                        devY(diagnoseCounter) = var(yAll);
                        if length(type) == 1
                            [type{1:length(x)}] = deal(tmpName{diagnoseCounter});
                        else
                            [type{length(type)+1:length(x)}] = deal(tmpName{diagnoseCounter});
                        end  
                    end
                    x = reshape(x, [size(x,2),size(x,1)]);
                    y = reshape(y, [size(y,2),size(y,1)]);
                    type = reshape(type, [size(type,2),size(type,1)]);
                    tmpColors = cell2mat(colors);
                    h = scatterhist(x, y,'Group', type,'Marker', 'x', 'Color', tmpColors(trueColors,:)); 
                    hists = [copy(h(2).Children), copy(h(3).Children)];
                    axes(h(2));
                    cla;
                    axes(h(3));
                    cla;
                    tmpDiag = unique(this.diagnoses.name); 
                    for j = 1:size(hists,1)
                        if this.vActualDistribution
                            inter = 'pchip';
                        elseif this.vGaussianDistribution
                            inter = 'linear';
                        end
                        ax = h(2);
                        axes(ax);
                        hist = hists(j,1);
                        if isstr(hist.FaceColor)
                            tmpCol = hist.EdgeColor;
                        else
                            tmpCol = hist.FaceColor;
                        end
                        yData = hist.BinCounts;
                        yData = interp1(1:length(yData), yData, 1:0.1:length(yData), inter);
                        xData = linspace(hist.BinLimits(1),hist.BinLimits(2),length(yData));
                        if this.vActualDistribution
                            plot(xData, yData, 'Color', tmpCol, 'LineWidth', 2);
                            hold on;
                        end
                        if this.vGaussianDistribution
                            sig = std(x(find(strcmpi(tmpDiag(find(ismember(cell2mat(colors),tmpCol, 'rows'))),type))));
                            mu = mean(x(find(strcmpi(tmpDiag(find(ismember(cell2mat(colors),tmpCol, 'rows'))),type))));
                            yStdDevData = normpdf(xData, mu, sig);
                            yStdDevData = yStdDevData * max(yData)/max(yStdDevData);
                            plot(xData, yStdDevData, 'Color', tmpCol, 'LineWidth', 2, 'Linestyle',':');
                            hold on;
                        end
                        ax = h(3);
                        axes(ax);
                        hist = hists(j,2);
                        yData = hist.BinCounts;
                        yData = interp1(1:length(yData), yData, 1:0.1:length(yData),inter);
                        xData = linspace(hist.BinLimits(1),hist.BinLimits(2),length(yData));
                        if this.vActualDistribution 
                            plot(xData, yData, 'Color', tmpCol, 'LineWidth', 2);
                            hold on;
                        end
                        if this.vGaussianDistribution
                            sig = std(y(find(strcmpi(tmpDiag(find(ismember(cell2mat(colors),tmpCol, 'rows'))),type))));
                            mu = mean(y(find(strcmpi(tmpDiag(find(ismember(cell2mat(colors),tmpCol, 'rows'))),type))));
                            yStdDevData = normpdf(xData, mu, sig);
                            yStdDevData = yStdDevData * max(yData)/max(yStdDevData);
                            plot(xData, yStdDevData, 'Color', tmpCol, 'LineWidth', 2, 'Linestyle',':');
                            hold on;
                        end
                    end
                    set(h(2),'Color','none');
                    set(h(3),'Color','none');
                    set(h(2),'Visible','off');
                    set(h(3),'Visible','off');
                    ax.YDir = 'reverse';
                    ax.View = [270, 90];
                    
                    this.psSet = this.currentStatisticalSet.Elements; 
                    this.psRef = this.currentStatisticalSet.getSeed;
                    this.psRefInit;
                end
                axes(h(1));
                this.hFeatureAxes = h(1);
                if all(ismember([1 2], this.plotSelection))
                    if sum(this.brushSelection) < this.vXAxis
                        xLabl = 'pressure ';
                        checkX = 0;
                    else
                        xLabl = 'flow ';
                        checkX = 1;
                    end
                    if sum(this.brushSelection) < this.vYAxis
                        yLabl = 'pressure ';
                        checkY = 0;
                    else
                        yLabl = 'flow ';
                        checkY = 1;
                    end
                else
                    if ismember(1, this.plotSelection)
                        checkX = 1;
                        checkY = 1;
                        xLabl = 'flow ';
                        yLabl = 'flow ';
                    else
                        checkX = 0;
                        checkY = 0;
                        xLabl = 'pressure ';
                        yLabl = 'pressure ';
                    end
                end
                if this.vAmplitudeSpec
                    xLabl = ['amplitude ',xLabl]; 
                    yLabl = ['amplitude ',yLabl]; 
                elseif this.vPhaseSpec
                    xLabl = ['phase ',xLabl]; 
                    yLabl = ['phase ',yLabl];
                else
                    if checkX
                        xLabl = [xLabl,'[ml/s]'];
                    else
                        xLabl = [xLabl,'[Pa]'];
                    end
                    if checkY
                        yLabl = [yLabl,'[ml/s]'];
                    else
                        yLabl = [yLabl,'[Pa]'];
                    end
                end
                if this.vGradientCompare
                    xLabl = ['gradient ',xLabl]; 
                    yLabl = ['gradient ',yLabl]; 
                end
                xlabel(gca, xLabl);
                ylabel(gca, yLabl);
                hold off;
            end
                
            function plotMean(varargin)
                axes(this.hFeatureAxes)
                l = size(meanX,2);
                hold on;            
                for counter = 1:l
                    scatter(meanX(counter), meanY(counter), 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor', [0 0 0]);
                end
            end      
            
            function plotDeviation(varargin)
                axes(this.hFeatureAxes)
                delete(findall(gcf,'type','annotation'));
                l = size(devX,2);
                hold on;
                xTick = get(gca,'XTick');
                yTick = get(gca,'YTick');
                xLims = get(gca,'XLim');
                yLims = get(gca,'YLim');
                scaleX = xTick(2)-xTick(1);
                scaleY = yTick(2)-yTick(1);
                L = norm([scaleX scaleY], 2);
                for counter = 1:l
                    x1 = meanX(counter);
                    y1 = meanY(counter);
                    x2 = meanX(counter)+devX(counter);
                    y2 = meanY(counter)+devY(counter);
                    dx = x2-x1;
                    dy = y2-y1;
                    nr = norm([dx, dy],2);
                    dx = (L/nr)*dx;
                    dy = (L/nr)*dy;
                    x2 = x1+dx; 
                    y2 = y1+dy;
                    x1 = x1-(0.45*(x2-x1));
                    x2 = x2-(0.45*(x2-x1));
                    y1 = y1-(0.45*(y2-y1));
                    y2 = y2-(0.45*(y2-y1));
                    line([x1 x2],[y1 y2],'Color','black', 'Linewidth', 2);
                end
            end
        end
        
        function data = operator(this, data)
            if this.vGradientCompare
                data = gradient(data);
            end
            if this.vAmplitudeSpec
                data = amplispec(data); 
            end
            if this.vPhaseSpec
                data = phasespec(data); 
            end
            if this.vPowerSpecCompare
                data = powerspec(data); 
            end

            function dta = powerspec(dat)
                N = length(n); 
                dta = abs(fft(dat))/N*2;   % absolute value of the fft
                dta = dta(1:ceil(N/2)).^2; %#ok<COLND> % find the corresponding frequency in Hz
            end

            function dta = amplispec(dat)
                n = length(dat);
                dat = dat - mean(dat);
                fftDat = fft(dat);
                freqSpec = abs(fftDat);
                freqSpec = 2*freqSpec/n;
                freqSpec(1) = freqSpec(1)/2;
                dta = freqSpec(1:floor(length(freqSpec)/this.div)); 
            end

            function dta = phasespec(dat)
                n = length(dat);
                dat = dat - mean(dat);
                fftDat = fft(dat)/n;
                phaseSpec = unwrap(angle(fftDat));
                phaseSpec(1) = phaseSpec(1)/n;
                dta = phaseSpec(1:floor(length(phaseSpec)/this.div));
            end    
        end
        
        function data = interpolateData(this, data)
            len = length(data);
            xAxis = linspace(1, len, this.refLen);
            data = interp1(1:len, data, xAxis);
        end
        
        function cbDelCompare(this, varargin)
            this.vDistinguishBetween   = 0;
            this.div                   = 5;
            this.vXAxis                = 1;
            this.vYAxis                = 1;
            this.vActualDistribution   = false;
            this.vGaussianDistribution = false;
            this.vLinkBrushUD          = false;
            this.vLinkBrushLR          = false;
            this.vGradientCompare      = false;
            this.vPowerSpecCompare     = false; 
            this.vAmplitudeSpec        = false; 
            this.vPhaseSpec            = false;
            this.vMean                 = false; 
            this.vDeviation            = false; 
            this.brushSelection        = [];
            delete(this.timerCellhCompare);
            delete(this.hCompare);
            this.hCompare = [];
            if ~isempty(this.hClassifierObj)
                this.hClassifierObj.cbDelClassifier;
            end
        end

    end % methods
    methods (Static)
        function ret = openDlg
            error('not implemented yet'); 
            ret = []; 
            scrsz = get(0,'ScreenSize'); 
            mp = get(0, 'MonitorPositions'); 
            if size(mp,1) > 1
                scrf = scrsz(3) + (scrsz(3)-1000)/2; 
            else
                scrf = (scrsz(3)-1000)/2; 
            end
            
            position = [scrf, (scrsz(4)-600)/2, 1200, 800]; 
            hDlg = figure('Name', title, 'NumberTitle', 'off', 'MenuBar','none', 'ToolBar','none', ...
                'Color', 'w', 'Position', position, 'Resize', 'off');

            % static text over panels
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'HorizontalAlignment', 'left', 'String', 'ArteryNet', ...
                  'Position', [20 770 300, 30], 'BackgroundColor', 'w'); 
            uicontrol(hDlg, 'Style', 'Text', 'FontSize', 14, 'String', 'StatisticalSet', ...
                  'Position', [20 370 300, 30], 'BackgroundColor', 'w', 'HorizontalAlignment', 'left'); 
            % buttons  
            hOk = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'Enable', 'off', 'String', 'Open', ...
                  'Position', [1050 20 60, 30], 'Callback', @cbOpen, 'KeyPressFcn', @cbOpen); 
              
            hCancel = uicontrol(hDlg, 'Style', 'Pushbutton', 'FontSize', 8, 'HorizontalAlignment', 'right', 'String', 'Cancel', ...
                  'Position', [1120 20 60, 30], 'Callback', @cbCancel); 
              
            table = javahandle_withcallbacks.javax.swing.JTable(dbcontent, headers); % R2014a/R2015a fixed
            table.setSelectionMode(0); 
            ScrollPane = javax.swing.JScrollPane(table);
            set(hDlg, 'Userdata', table); 
            btLinePropsPos = [[0.02 0.52],0.96,0.45];
            [~,btContainer] = javacomponent(ScrollPane,[0 0 1 1], hDlg); 
            set(btContainer, 'Units', 'Norm', 'Position', btLinePropsPos);
            
            


            function cbSelection
                warning('off', 'MATLAB:hg:JavaSetHGProperty'); 
                set(table,'MousePressedCallback',@aOpenDlgSelection);
                set(table,'KeyPressedCallback',@aOpenDlgSelection);
                set(table, 'MouseMovedCallback', {@aOpenDlgMouseMoved, 1});  % for tooltip table 1
            end
        end
    end
    
end

